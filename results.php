<?php
session_start();
include 'readcookie.php';
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	header("Location:index.php");
}
else if(isset($_POST['searchquery'])){
	include 'scripts/connect.php';
	$search = $_POST['searchquery'];
	$results = array();
	$newsearch = "%".$search."%";
	$wordsearch = str_replace(" ","%", $search);
	$wordsearch = "%".$wordsearch."%";
	$keywords = explode(" ", $search);
	$length = count($keywords);
	$regexpsearch = "^.*(";
	for($i = 0; $i < $length; $i++){
		$regexpsearch.= $keywords[$i]."|";
	}
	$regexpsearch = substr($reg, 0,-1);
	$regexpsearch.= ").*$";
	
	$query = "SELECT user_id, user_name,user_email,user_dp FROM mashup_users WHERE user_verified = 1 AND (user_email LIKE ? OR user_name LIKE ?)";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($newsearch,$newsearch));
	if($stmt->rowCount() >= 1){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			array_push($results, $temp);
		}
	}
	$query = "SELECT user_id, user_name,user_email,user_dp FROM mashup_users WHERE user_verified = 1 AND (user_email LIKE ? OR user_name LIKE ?)";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($wordsearch,$wordsearch));
	if($stmt->rowCount() >= 1){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			array_push($results, $temp);
		}
	}
	$query = "SELECT user_id, user_name,user_email,user_dp FROM mashup_users WHERE user_verified = 1 AND (user_email REGEXP ? OR user_name REGEXP ?)";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($regexpsearch,$regexpsearch));
	if($stmt->rowCount() >= 1){
		while($temp = $stmt->fetch(PDO::FETCH_ASSOC)){
			array_push($results, $temp);
		}
	}
	$results = array_map("unserialize", array_unique(array_map("serialize", $results)));
	include 'head.php';
	?>
	<title>Search Results</title>
	<style>
		.col1{
			width:10%;
		}
		.col2{
			width:90%;
		}
	</style>
	</head>
	<body class='theme-pattern-lightmesh'>
		<?php include 'header.php';?>
		<div id = "content" id = "main">
			<section class = "section alt">
				<div class = "container">
					<div class = "row">
						<h1 class = "pull-left">Search Results for '<span style = "font-weight:bold"><?php echo $search;?></span>':</h1>
					</div>
				</div>
			</section>
			<section>
				<div class = "container">
					<div class = "row">
					<table class = "table table-hover">
					<?php foreach($results as $key=>$result){
							$image = stripslashes($result['user_dp']);
							if($image == "" || $image == "NULL"){
								$image = "nodp.gif";
							}
							$name = $result['user_name'];
							$email = $result['user_email'];
							$id = $result['user_id'];
							if($id == $_SESSION['user_id'])
								continue;
							?>
							<tr>
								<td class = 'col1'>
									<a href = "profile.php?user=<?php echo $id;?>">
									<img src = 'images/<?php echo $image;?>' height = "100" width = "100"></img>
									</a>
								</td>
								<td class = 'col2'><h3><?php echo $name?></h3><p><?php echo $email?></p></td>
							</tr>
								
							<?php 
						}
					?>
					</table>
					</div>
				</div>
			</section>
		</div>
	</body>
	<?php 
}
else{
	header("Location:reader.php");
}
?>