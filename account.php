<?php
	session_start();
	include 'readcookie.php';
	if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
		header("Location:index.php");
	}
	if(isset($_POST['email']) && isset($_POST['username']) && isset($_POST['bio'])){

		include 'scripts/connect.php';
		if(isset($_FILES["file"])){

			if ($_FILES["file"]["error"] > 0)
			{
				$filename = "";
			}
			else
			{
				if (file_exists("../images/" . $_FILES["file"]["name"]))
				{
			
				}
				else
				{
					move_uploaded_file($_FILES["file"]["tmp_name"],
							"images/" . $_FILES["file"]["name"]);
					//echo "Stored in: " . "images/" . $_FILES["file"]["name"];
					$filename = $_FILES['file']['name'];
				}
			}	
		}
		else{
			$filename = "";
		}
// 		echo "<br/>Filename: $filename<br/>";
		
		$email = $_POST['email'];
		$name = $_POST['username'];
		$bio = $_POST['bio'];
		$userid = $_SESSION['user_id'];
// 		echo $userid;
		$query = "SELECT * FROM mashup_users WHERE user_id = '$userid'";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to interact with database");
		}
		else{
			$temp = mysql_fetch_assoc($result);
			$emailold = $temp['user_email'];
			if($filename == ""){
				$filename = $temp['user_dp'];
			}
		}
// 		echo $filename." $email $name $bio $emailold";
		if($emailold == $email){
// 			echo "No change";
			$query = "UPDATE mashup_users SET user_name = ?, user_bio = ?, user_dp = ? WHERE user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($name,$bio,$filename,$userid));
			$_SESSION['username'] = $name;
		}
		else{
// 			echo "Change";
			$str = "$email$name";
			$hash = md5($str);
			$query = "UPDATE mashup_users SET user_email_pending = ?, user_name = ?, user_bio = ?, user_dp = ?, user_verification_id = ? WHERE user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($email,$name,$bio,$filename,$hash,$userid));
			$subject = "[Mashup] Verification of new email id for $name";
			$message = "You changed your email id recently\n".
					"Please click on the link below to verify your account, you won't be able\n".
					"to use this email id:\n".
					"http://www.foreverdope.com/mashup/scripts/verifyemailchange.php?userid=".urlencode($userid)."&hash=$hash\n";
			$from = "no-reply@mashup.foreverdope.com";
			$headers = "From:" . $from;
			mail($email, $subject, $message,$headers);
		}
		header("Location:account.php");
	}
	else{
		include 'scripts/connect.php';
		$user_id = $_SESSION['user_id'];
		$name = $_SESSION['username'];
		$query = "SELECT * FROM mashup_users WHERE user_id = '$user_id'";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to query database");
		}
		else{
			$temp = mysql_fetch_assoc($result);
			$email = $temp['user_email'];
			$bio = stripslashes($temp['user_bio']);
			$image = stripslashes($temp['user_dp']);
			if($image == "" || $image == "NULL"){
				$image = "nodp.gif";
			}
			$emailpending = $temp['user_email_pending'];
			include 'head.php';?>
			<script type = "text/javascript" src = "js/ajaxfileupload.js"></script>
			<script type = "text/javascript">
				$(document).ready(function(){

					$("body").keypress(function(event){
						if(event.which == 13){
							if($("#updatePassword:visible").length>0){
								$("#updatePassword").trigger("click");
							}
						}
					});
					$("#passwordmodal").on("hidden",function(){
						$("#success,#error,#loading").hide();
						$("#changePasswordForm").trigger("reset").show();
					});
					$("#passchange").click(function(){
						$("#passwordmodal").modal('show');
						return false;
					});
					$("#cancel").click(function(){
						$("#passwordmodal").modal('hide');
						return false;
					});

					$("#updatePassword").click(function(){
						$("#changePasswordForm").trigger("submit");
					});
					$("#changePasswordForm").submit(function(){
						
						currentpass = $("input[name = 'currentPass']").val();
						newpass = $("input[name = 'newPass']").val();
						confirmpass = $("input[name = 'confirmPass']").val();
						if(currentpass == ""||newpass == ""||confirmpass==""){
							alert("Please complete all fields first");
							return false;
						}
						else if(newpass!=confirmpass){
							alert("Passwords do not match.");
						}
						else{
							$("#error,#success").slideUp();
							$(this).slideUp("300",function(){
								$("#updatePassword").hide();
								$("#loading").fadeIn("200");
							});
							var data = $(this).serialize();
							$.ajax({
								url:"scripts/changepassword.php",
								type:"POST",
								data:data,
								dataType:"JSON",
								success:function(response){
									if(response.status == "unauthenticated"){
										window.location = "index.php";
									}
									if(response.status == "Success"){
										$("#success p").html(response.message);
										$("#loading").fadeOut("300",function(){
											$("#success").show();
										});
									}
									else{
										$("#error p").html(response.message);
										$("#loading").fadeOut("300",function(){
											$("#error,#changePasswordForm,#updatePassword").show();
										});
									}
								},
								error:function(){
									$("#loading").fadeOut(300,function(){
										$("#error p").html("Error");
										$("#error,#changePasswordForm,#updatePassword").show();
									});
								}
							});
						}
						return false;
					});
				});
			</script>
			<style>
				#error,#success{
					display:none;
				}
			</style>
			<title>My Account</title>
			<link href="css/bootstrap.min-jasny.css" rel="stylesheet" type="text/css" />
			<script type = "text/javascript" src = "js/bootstrap.min-jasny.js"></script>
			</head>
			<body class = "theme-pattern-lightmesh">
			<?php include 'header.php';?>
			<section class = "section alt">
				<h1 class = 'pull-center header'>My Account</h1>			
			</section>
			<section class = "section">
				<div class = "container">
					<div class = "row">
					
					<form enctype="multipart/form-data" id = "details_form" action = "" method = "POST">
					<div class = "span3 offset2">
						<h3 class = "pull-left">Profile Picture</h3>
						<!-- Image uploading Widget from Bootstrap Jasny-->
								
								<div class="fileupload fileupload-new pull-left" style = "position:relative;top:7px;left:10px"  data-provides="fileupload">
								  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="images/<?php echo $image; ?>"/></div>
								  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
								  <div>
								    <span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
								    <input id = "profiledp" name = "file" type="file" /></span>
								    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
								  </div>
								</div>
								<br/>
								
								<!-- End of Image Upload Widget -->
					</div>
					<div class = "span5">
						<label>Name:</label><input type = "text" class = "span5" name = "username" value = "<?php echo $name; ?>"/><br/>
						<label>Email:</label><input type = "text" class = "span5" name = "email" value = "<?php echo $email; ?>"/><br/>
						<?php
							if($emailpending != ""){
								?>
							<div class = "alert alert-warning span5" style = "margin-left:0px;">
								<p>You have not verified <?php echo $emailpending;?>. Verify the email address to activate it</p>
							</div>
								<?php 
							} 
						?>
						<label>Password:</label>
						<button id = "passchange" style = "margin-left:0px" class = "btn btn-blue span5 btn-block">Change Password</button>
						<br/>
						<label>Bio:</label><textarea rows = "3" cols = "100" class = "span5" name = "bio"><?php echo $bio;?></textarea><br/>
						<input type = "submit" class = "btn btn-large btn-primary pull-right" value = "Submit"/><br/>
					</div>
					</form>
					
					</div>
				</div>
				</section>
				<div class="modal hide fade" id = "passwordmodal">
					<div class="modal-header">
				    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				    	<h3 class = "pull-center">Change your password</h3>
				  	</div>
				  	<div class = "modal-body">
				  	<div class = "alert alert-success" id = "success">
				  		<p class = "pull-center"></p>
				  	</div>
				  	<div class = "alert alert-danger" id = "error">
				  		<p class = "pull-center"></p>
				  	</div>
				  	<div class = "pull-center" id = "loading" style = "margin-bottom:15px;display:none;">
			  			<img src = "images/ajax-loader.gif"></img>
			  		</div>
				  		<form id = "changePasswordForm" class = "pull-center">
				  			<label class = "pull-left">Current Password:</label>
				  			<input type = "password" class = "span5" placeholder = "Current Password" name = "currentPass"><br/>
				  			<label class = "pull-left">New Password:</label>
				  			<input type = "password" class = "span5" placeholder = "New Password" name = "newPass"><br/>
				  			<label class = "pull-left">Confirm Password:</label>
				  			<input type = "password" class = "span5" placeholder = "Confirm Password" name = "confirmPass"><br/>
				  		</form>
				  	</div>
				  	<div class = "modal-footer">
				  		<a href="#" class="btn pull-right signup" id = "cancel">Cancel</a>
  						<a href="#" style = "margin-right:5px" class="btn btn-primary pull-right signup" id = "updatePassword">Update</a>
				  	</div>
				  </div>
			</body>
			<?php 	
		}
	}
?>