<?php
session_start();
include 'readcookie.php';
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	header("Location:index.php");
}
else{
	include 'scripts/connect.php';
	$userid = $_SESSION['user_id'];
	$query = "SELECT user_id,user_dp,user_name,friends_user_id FROM mashup_users u,mashup_friends_$userid f".
	" WHERE u.user_id = f.friends_user_id";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to query databases");
	}
	else{
		$friends = array();
		if(mysql_num_rows($result) == 0){
			$zerofriends = true;
		}
		else{
			while($temp = mysql_fetch_assoc($result)){
				array_push($friends, $temp);
			}
		}
		include 'head.php';
	}
	?>
	<title>Friends</title>
	<script type="text/javascript" src="js/jquery.loadmask.min.js"></script>
	<link href="css/jquery.loadmask.css" rel="stylesheet" type="text/css" />
	<script type = "text/javascript">
		$(document).ready(function(){
			$("body").ajaxStart(function(){
				$("#successdiv,#errordiv").slideUp("200");
			});
			$(".unfriend").click(function(){
				id = this.id.substr(4);
				action = "delete";
				data = {"friend_id":id,"action":"delete"};
	        	$(this).mask("Removing...",200)
	        	$.ajax({
	        		url:"scripts/friendaction.php",
	        		data:data,
	        		type:"POST",
	        		dataType:"JSON",
	        		success:function(data){
	        			if(data.status == "unauthorized"){
	        				window.location = "index.php";
	        			}
	        			else if(data.status == "success"){
	        				$("#successdiv p").html("Friend Succesfully Removed").parent()
	        				.slideDown("200",function(){
	        					window.location = "";
		        			});
	        				
	        			}
	        			else{
	        				$("#errordiv p").html(data.message).parent()
	        				.slideDown("200",function(){
	        					$(".unfriend").unmask();
	        					
		        			});
	        			}
	        		},
	        		error:function(){
	        			$("#errordiv p").html("Error Occurred").parent()
        				.slideDown("200",function(){
        					$(".unfriend").unmask();
        					
	        			});
	        		}
	        	});
	        	return false;
			});
		});
	</script>
	<style>
		#successdiv,#errordiv{
			display:none;
		}
	</style>
	</head>
	<body class='theme-pattern-lightmesh'>
	<?php include 'header.php';?>
	<div id = "content" role = "main">
	<section class = "section alt" id = "promo" style = "padding-top:10px;padding-bottom:10px">
		<div class = "container">
			<div class = "row">
					<h1 class = "pull-center">Following</h1>
			</div>
		</div>
	</section>
	<section class = "section">
	<div class = "container">
		<div class = "row">
		<div class = "span12 alert alert-success" id = "successdiv">
			<p class = "pull-center"></p>
		</div>
		<div class = "span12 alert alert-danger" id = "errordiv">
			<p class = "pull-center"></p>
		</div>
		<?php if(isset($zerofriends)){
		?>
			<legend style = "position:relative;left:8%"> You are not following anyone yet. Use the search bar to find people</legend>
		<?php }
		else{?>
			<ul class='thumbnails bordered thumbnail-list'>
				<?php
				$len = count($friends);
				for($i = 0; $i < $len; $i++){
					$img = stripslashes($friends[$i]['user_dp']);
					$name = $friends[$i]['user_name'];
					$id = $friends[$i]['user_id'];
					echo '<li "storygrid span3" style ="padding:0px;"><div class = "headers" style = "background-color:lightblue;border-top-left-radius:5px;border-top-right-radius:5px;">'.
					'<div class = "pull-center"><h2 class = "pull-center">'.$name.'</h2></div>';
					if($img == ""||$img == "NULL"){
						$src = "images/nodp.gif";
					}
					else{
						$src = "images/$img";
					}
					echo "<a href = 'profile.php?user=$id'>".
							"<figure class='thumbnail-figure' style = 'margin-bottom:5px;'>".
							"\n<img src = '$src' width = '280px' height = '180px'".
							" style = 'height:180px;width:280px'></img>".
							"</figure></a>\n</div>".
							"<a href = '#' class = 'btn btn-danger pull-right unfriend' id = 'user$id' style = 'margin-right:5px;margin-left:5px;'>Unfollow</a>".
							"</li>";
					if(($i-3)%4 ==0){
						echo "</ul><ul class = 'thumbnails bordered thumbnail-list'>";
					}
				} 
				?>
			</ul>
		<?php }?>
		</div>
		</div>
	</section>
	</div>
	</body>
	</html>
	<?php 
}
?>