<?php
	session_start();
	include 'readcookie.php';
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
		header("Location:index.html");
	}
	else{
		include 'scripts/connect.php';
		$id = $_SESSION['user_id'];
		$query = "SELECT * FROM mashup_magazines_$id ORDER BY magazines_timeofadding DESC";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to query for magazines");
		}
		else{
			if(mysql_num_rows($result) >=1){
				$temp = mysql_fetch_assoc($result);
				$magazineaddedid = $temp['magazines_id'];
				$done = "website";
				$query1 = "SELECT * FROM mashup_sites_$id";
				$result1 = mysql_query($query1);
				if(!$result1){
					die("Unable to Query for sites");
				}
				else{
					if(mysql_num_rows($result1)>=1){
						$done = "personal";
					}
					$query2 = "SELECT * FROM mashup_users WHERE user_id = '$id'";
					$result2 = mysql_query($query2);
					if(!$result2){
						die("Unable to query for biodata");
					}
					else{
						$temp = mysql_fetch_assoc($result2);
						if($temp['user_bio']!=""){
							header("Location:reader.php");
						}
					}
				}
			}
?>
	<?php include 'head.php'?>
	<title>Complete your profile</title>
    <link href="css/bootstrap.min-jasny.css" rel="stylesheet" type="text/css" />
    <script type = "text/javascript" src = "js/bootstrap.min-jasny.js"></script>
    <script type = "text/javascript">
    	var done ='<?php echo $done?>';
    	var magazineid = '<?php if(isset($magazineaddedid)) echo $magazineaddedid?>';
    </script>
    <script type = "text/javascript" src = "js/234.js"></script>
    <script type = "text/javascript" src = "js/ajaxfileupload.js"></script>
    <style>
    	.loadingimg,.successimg,.failureimg{
    		display:none;
    	}
    </style>
  </head>
  <body class='theme-pattern-lightmesh'>
  
    <!-- Page Header -->
    <header id='masthead'>
      <nav class='navbar navbar-fixed-top'>
        <div class='navbar-inner'>
          <div class='container'>
            <a class='btn btn-navbar' data-target='.nav-collapse' data-toggle='collapse'>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
            </a>
            <h1 class='brand' style = "padding-top:5px;padding-bottom:5px;margin-left:25px;">
              <a href='index.php' style = "position:relative;top:10px;"><img id = "gridfeed" style = 'position:relative;bottom:10px;'src = "images/gridfeeds.png" height = "70px" width = "70px"></img></a>
            </h1>
            <div class = "pull-right">
			<div class = "btn-group pull-right" style = "margin-right:50px">
            	<button class = "btn btn-inverse dropdown-toggle" data-toggle = "dropdown">
            	<i class = "icon-cog"></i>
            	</button>
            	<ul class = "dropdown-menu pull-center">
            		<li class = "dropdown-caret right">
				  		<span class = "caret-outer"></span>
				  		<span class = "caret-inner"></span>
			  		</li>
	            	<li><a href = "scripts/logout.php">Logout</a></li>
            	</ul>
            </div>
			</div>
            <div class='nav-collapse'>
            <ul class = "nav pull-right">
            </ul>
               
            </div>
          </div>
        </div>
      </nav>
    </header>
    <!--  End of Header -->
    
    <!--  Main Content -->
    <div class = "content" role = "main">
    	<div class = "container">
    		<div class = "span8 offset2">
    			<div class = "alert alert-danger" id = "errordiv" style = "display:none">
    			</div>
    			<div class = "alert alert-success" id = "successdiv" style = "display:none">
    			</div>
    			<section class = "section alt" id = "mainBox">
    			
    				<div class="progress progress-info progress-striped" style = "margin-left:10px;margin-right:10px;">
  						<div class="bar" style="width:1%;"></div>
					</div>
					
					<div class = "infoBox">
					<!-- Loading Div-->
					<div class = "pull-center" id = "loading" style = "display:none">
			  			<img src = "images/ajax-loader.gif"></img>
			  		</div>
			  		<!-- End of Loading Div -->
						<!-- Form for creating Magazine -->
						<h1 class = "pull-center" id = "magazineHeader">Create Your First Magazine</h1>
						<form id = "magazineForm" style = "position:relative;left:50px;top:40px;">
							<i class = "icon-plus"></i> <input class = "span7" type ="text" name = "magazinename"/><br/>
							<input type = "submit" value = "Next" class = "btn btn-large btn-primary pull-right"
							style = "position:relative;right:85px;top:25px">
						</form>
						<!-- End of Form -->
						
						<!-- Form for adding Websites -->
						<h1 class = "pull-center" id = "websiteHeader" style = "display:none;">Add Your Favorite Websites</h1>
						<form id = "websiteForm" style = "position:relative;left:50px;top:20px;display:none">
							<i class = "icon-plus"></i> <input class = "span7 siteurl" type ="text" name = "siteurl[]"/>
								<img class = "loadingimg" src = "images/ajax-loader-arrows.gif"></img>
								<a href = "#" class = "tooltiptrigger" title = "#" rel = "tooltip"><img class = "failureimg" src = "images/cross.png"></img></a>
								<a href = "#" class = "tooltiptrigger" title = "#" rel = "tooltip"><img class = "successimg" src = "images/green_check_alt.png" height = "20px" width = "20px"></a>
								</img>
								<br/>
							<i class = "icon-plus"></i> <input class = "span7 siteurl" type ="text" name = "siteurl[]"/>
								<img class = "loadingimg" src = "images/ajax-loader-arrows.gif"></img>
								<a href = "#" class = "tooltiptrigger" title = "#" rel = "tooltip"><img class = "failureimg" src = "images/cross.png"></img></a>
								<a href = "#" class = "tooltiptrigger" title = "#" rel = "tooltip"><img class = "successimg" src = "images/green_check_alt.png" height = "20px" width = "20px"></a>
								</img>
								<br/>
							<i class = "icon-plus"></i> <input class = "span7 siteurl" type ="text" name = "siteurl[]" class = "siteurl"/>
								<img class = "loadingimg" src = "images/ajax-loader-arrows.gif"></img>
								<a href = "#" class = "tooltiptrigger" title = "#" rel = "tooltip"><img class = "failureimg" src = "images/cross.png"></img></a>
								<a href = "#" class = "tooltiptrigger" title = "#" rel = "tooltip"><img class = "successimg" src = "images/green_check_alt.png" height = "20px" width = "20px"></a>
								</img></a>
								<br/>
							<i class = "icon-plus"></i> <input class = "span7 siteurl" type ="text" name = "siteurl[]" class = "siteurl"/>
								<img class = "loadingimg" src = "images/ajax-loader-arrows.gif"></img>
								<a href = "#" class = "tooltiptrigger" title = "#" rel = "tooltip"><img class = "failureimg" src = "images/cross.png"></img></a>
								<a href = "#" class = "tooltiptrigger" rel = "tooltip"><img class = "successimg" src = "images/green_check_alt.png" height = "20px" width = "20px"></a>
								</img>
								<br/>
								<input type = "hidden" id = "magazineid" value = "0" name = "magazine_id"/>
							<input type = "submit" value = "Next" class = "btn btn-large btn-primary pull-right"
							style = "position:relative;right:85px;top:10px;">
						</form>
						<!-- End of Form -->
						
						<!-- Completion Messages -->
						<h1 class = "pull-center" id = "completeHeader" style = "display:none;">Congratulations!!!</h1>
						<h3 class = "completeText pull-center"style = "display:none;">Your profile is now complete</h3>
						<div class = "span3 offset2"  id = "headInside" style = "display:none;position:relative;top:60px;left:20px">
						<a href = "reader.php" class = "btn btn-block btn-large pull-center" >Head Inside!</a>
						</div>
						<!-- End of Completion Messages -->
						
						<!-- Form for Personal Details -->
						<h1 class = "pull-center" id = "personalHeader" style = "display:none;top:-10px">Your Profile</h1>
						<section class = "span8 section alt" id = "personalForm" style = "display:none;padding-top:5px;padding-bottom:5px;">
							<div class = "container">
								<div class = "row-fluid">
								<div class = 'span12'>
									<h3 class = "pull-left" style = "margin-left:30px">Complete Your Profile:</h3>
								</div>
							<form  class = "span12">
							<div class = "row-fluid">
							<!-- Image uploading Widget from Bootstrap Jasny-->
							
							<div class="span3 fileupload fileupload-new pull-left" data-provides="fileupload">
							  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
							  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
							  <div>
							    <span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input id = "profiledp" name = "file" type="file" /></span>
							    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
							  </div>
							</div>
							
							<!-- End of Image Upload Widget -->
							
							<div class = "span7" style = "margin-left:-20px;">
								<textarea rows="7" id = "bio"  class = "span8" placeholder="Add your Bio (140 characters max):"></textarea>
							</div>
							</div>
							<div class = "span7">
								<input type = "submit" value = "Upload" style = "margin-right:5px;margin-top:-10px" class = "pull-right btn btn-large btn-primary pull-right"/>
							</div>
							</div>
							</div>
							</form>
								</div>
							</div>
						</section>
						<!-- End of Form -->
						
					</div>
    			</section>
    		</div>
    	</div>
    </div>
    <!--  End of Main Content -->
</body>
</html> 
<?php
			
		}
	} 
?>