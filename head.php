<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset='utf-8' />
    <!-- %meta{:content => "IE=edge,chrome=1", "http-equiv" => "X-UA-Compatible"} -->
    <meta content='Bootsrap based theme' name='description' />
    <meta content='width=device-width' name='viewport' />
    <!--[if lt IE 9]>
      <script src='http://html5shim.googlecode.com/svn/trunk/html5.js'></script>
    <![endif]-->
    <link href='images/favicon.ico' rel='shortcut icon' />
    <link href="stylesheets/bootstrap.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="stylesheets/responsive.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="stylesheets/font-awesome.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="stylesheets/theme.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="stylesheets/fonts.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" />
    <script type = "text/javascript" src = "js/jquery.js"></script>
    <script type = "text/javascript" src = "js/bootstrap.min.js"></script>