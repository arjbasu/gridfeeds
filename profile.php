<?php
session_start();
include 'readcookie.php';
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	header("Location:index.php");
}
else{
	include 'scripts/connect.php';
	$userid = $_SESSION['user_id'];
	if(isset($_GET['user'])){
		$display = $_GET['user'];
		if($display == $userid){
			$friendbutton = false;
		}
		else{
			$query = "SELECT magazines_name, magazines_id FROM mashup_magazines_$userid";
			$result = mysql_query($query);
			if(!$result){
				die("Unable to query database");
			}
			else{
				$magazineown = array();
				while($temp = mysql_fetch_assoc($result)){
					array_push($magazineown,$temp);
				}
			}
			$query = "SELECT * FROM mashup_friends_$userid WHERE friends_user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($display));
			if($stmt->rowCount()!=1){
				$friends = false;
			}
			else{
				$friends = true;
			}	
		}
		
	}
	else{
		$display = $_SESSION['user_id'];
		$friendbutton = false;
	}
	$query = "SELECT * FROM mashup_users WHERE user_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($display));
	if($stmt->rowCount() != 1){
		header("Location:index.php");
	}
	else{
		$temp = $stmt->fetch(PDO::FETCH_ASSOC);	
		$image = stripslashes($temp['user_dp']);
		if($image == ""||$image == "NULL"){
			$image = "nodp.gif";
		}
		$name = $temp['user_name'];
		$bio = stripslashes($temp['user_bio']);
		$tablename = "mashup_magazines_$display";
		$query = "SELECT * FROM $tablename";
		$result = mysql_query($query);
		if(!$result){
			die("Unable to query databases");
		}
		else{
			$magazines = array();
			
			while($temp = mysql_fetch_assoc($result)){
				$temparray = array();
				$magname = $temp['magazines_name'];
				$magid = $temp['magazines_id'];
				$temparray['magname'] = $magname;
				$temparray['sites'] = array();
				$magazines[$magid] = $temparray;
			}
			
			$query = "SELECT * FROM mashup_sites_$display";
			$result = mysql_query($query);
			
			if(!$result){
				die("Unable to query database");
			}
			
			else{
				while($temp = mysql_fetch_assoc($result)){
					$magazineid = $temp['sites_magazine_id'];
					array_push($magazines[$magazineid]['sites'], $temp);
				}
				
				include 'head.php';
				$title = "$name's Profile"
				?>
				<title><?php echo $title;?></title>
				<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
				<script type = "text/javascript" src = "js/jquery-ui-1.9.1.custom.min.js"></script>
			    <script type = "text/javascript" src = "js/jquery.mousewheel.min.js"></script>
			    <script type = "text/javascript" src = "js/jquery.mCustomScrollbar.js"></script>
				<script type = "text/javascript" src = "js/profile.js"></script>
				<style>
				.contents{
					height:200px;
					background-color:#EFEFEF;
					padding:5px 10px;
					border-bottom-left-radius:7px;
					border-bottom-right-radius:7px;
				}
				#magazines{
					margin-top:20px;
					margin-bottom:40px;
				}
				.magazinedetails{
					background-color:lightblue;
					border-radius:7px;
					margin-bottom:20px;
				}
				</style>
				<script type="text/javascript" src="js/jquery.loadmask.min.js"></script>
				<link href="css/jquery.loadmask.css" rel="stylesheet" type="text/css" />
				<script type = "text/javascript">
				self = true;
				<?php
					if(!isset($friendbutton)){ 
				?>
				
					var magazines = <?php echo json_encode($magazineown);?>;
					//console.log(magazines);
					self = false;
					<?php }?>
					var magazinelist = <?php echo json_encode($magazines);?> 
					</script>
				</head>
				<body class='theme-pattern-lightmesh'>
					<?php include 'header.php';?>
					<div id = "content" role = "main">
					<section class = "section">
						<div class = "container">
							<div class = "row">
								<div id = "successdiv" class = "alert alert-success" style = "display:none">
									<p class = "pull-center"></p>
								</div>
								<div id = "errordiv" class = "alert alert-danger" style = "display:none">
									<p class = "pull-center"></p>
								</div>
							</div>
						</div>
					</section>
					<section class = 'section alt' id = 'promo' style = "padding-top:15px;padding-bottom:10px">
					<div class = 'container'>
						<div class = 'row'>
							<div id = "dpspot" class = "span3 pull-center" style = "margin-left:60px">
							  	<img id = "dp" onload = "adjust()" class = 'img img-polaroid pull-left' src = 'images/<?php echo $image;?>' style = "visibility:hidden;height:180px"></img>
							</div>
							<div id = "profiledetails" class = "span4" style = "margin-left:5px;visibility:hidden;position:relative">
								<h2><?php echo $name;?></h2>
								<p><?php echo $bio;?>
							</div>
							<?php if(!isset($friendbutton)){?>
							<div class = "span2 offset10">
								<?php if($friends == false){?>
								<button class = "btn btn-large btn-primary friendAdd" id = "user<?php echo $display;?>" style = "position:relative;bottom:20px">
								Follow</button>
								<?php }
								else{
									?>
									<button class = "btn btn-large btn-success friendRemove" id = "user<?php echo $display;?>" style = "position:relative;bottom:20px">
									Following</button>
									<?php 
								}?>
							</div>
							<?php }?>
						</div>
					</div>
					</section>
					<div class = "container">
						<div class = "row">
							<div class = "span12">
								<div class = "span12" id = "magazines">
								<div class = "row">
									<?php
									
										foreach($magazines as $key=>$magazine){
											?>
											<div class = "span3 magazinedetails" id = "magazine<?php echo $key;?>">
												<h3 class = 'pull-center'><?php echo $magazine['magname'];?></h3>
												<div class = 'contents'>
													<?php
													foreach($magazine['sites'] as $index=>$site){
													?>
													<p class = 'site' id = '<?php echo $index;?>'>
													<?php
							    						if($site['sites_name'] == "")
							    							echo $site['sites_url'];
							    						else
							    							echo $site['sites_name']; 
							    					?>
							    					</p>
													<?php 
													} 
													?>
												</div>
											</div>
											<?php 
										} 
									?>
								</div>
								</div>
							</div>
						</div>
					</div>
					</div>
					<div class="modal hide fade" id = "magazineSelect">
						  <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						    <h2 class = "pull-center">Select a Magazine</h2>
						  </div>
						  <div class="modal-body">
						    <div id = "loading" style = "display:none;">
						    	<img src = "images/ajax-loader.gif" class = "pull-center"></img>
						    </div>
							    <div class = "error alert alert-danger" style = "display:none">
							  	<p class = "pull-center"></p>
							  </div>
							  <div class = "success alert alert-success" style = "display:none">
							  	<p class = "pull-center"></p>
							  </div>
						    <div id = "magazinelist">
						    	<form id = "magazinelistForm">
						    		<?php foreach($magazineown as $id=>$value){
						    		?>
						    		<button type = "button" class = "btn btn-primary formmagazinename" 
						    		id = "mag<?php echo $value['magazines_id'];?>" style = "margin:5px">
						    		<?php echo $value['magazines_name'];?></button>
						    		<input type = 'hidden' name = 'siteurl' id = 'formsiteurl' value = ""/>
						    		<?php }?>	
						    	</form>
						    </div>
						   </div>
						   <div class = "modal-footer">
						   	<a href = "#magazineSelect" data-dismiss="modal" class = "pull-right btn">Close</a>
						   </div>
					</div>					 
				</body>
			 	</html>
				<?php 
			}
		}
	}
	
}
?>