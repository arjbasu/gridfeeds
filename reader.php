<?php
session_start();
include 'readcookie.php';
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	header("Location:index.php");
}
else{
	include 'scripts/connect.php';
	require_once('php/autoloader.php');
	$user_id = $_SESSION['user_id'];
	$tablename = "mashup_magazines_$user_id";
	$query = "SELECT * FROM $tablename WHERE magazines_site_count>0";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to make database connection");
	}
	else{
		if(mysql_num_rows($result) == 0){
			header("Location:234.php");
		}
		else{
			$magazines = array();
			while($temp = mysql_fetch_assoc($result)){
				$temparray = array();
				$magname = $temp['magazines_name'];
				$magid = $temp['magazines_id'];
				$temparray['magname'] = $magname;
				$temparray['sites'] = array();
				$magazines[$magid] = $temparray;
			}
			$query = "SELECT * FROM mashup_sites_$user_id";
			$result = mysql_query($query);
			if(!$result){
				die("Unable to query database");
			}
			else{
				while($temp = mysql_fetch_assoc($result)){
					$magazineid = $temp['sites_magazine_id'];
					array_push($magazines[$magazineid]['sites'], $temp);
				}
				include 'head.php';
			}
		}
	}
} 
?>
    <link rel="stylesheet" type="text/css" href="./fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <style>
    	.controls{
    		display:none;
    	}
    	.sidePane{
			position:fixed;
			top:85px;
		}
		
		.sidePane #headersection{
			padding-top:10px;
			padding-bottom:10px;
			border-radius:3px;
		}
		
		.mainContent{
			position:relative;
			margin-top:-50px;
		
		}
		#lightbox-title{
			background-color:lightblue;
			border-top-left-radius:5px;
			border-top-right-radius:5px;
		}
		#lightbox-source{
			background-color:lightblue;
			margin-bottom:10px;
		}
    </style>
    <script type = "text/javascript" src = "js/jquery-ui-1.9.1.custom.min.js"></script>
	<script type = "text/javascript" src = "js/jquery.mCustomScrollbar.js"></script>
    <script type = "text/javascript" src = "js/reader.js"></script>
	<script type="text/javascript" src="./fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<title>Dashboard</title>
  </head>
  <body class='theme-pattern-lightmesh'>
    <!-- Page Header -->
    <?php include 'header.php';?>
    <!-- Main Content -->
	    <div class = "container-fluid">
	    <div class = "row-fluid">
	    	<div class = "span12" id = "errordiv" style = "display:none">
	    	<p class = "pull-center alert alert-danger"></p>
	    	</div>
	    </div>
	    <div class = "row-fluid">
	    <!--  Side Pane Content-->
	    	<div class = "span3 sidePane ">
		    	<div class = "magazines">
		    	
			    	<section class = "section alt" id = 'headersection'>
			    		<h2 class = "pull-center">Magazines</h2>
			    	</section>
			    	
			    	<ul class="nav nav-pills nav-stacked" style = "position:relative;top:10px;">
				    	<?php
				    		foreach($magazines as $key=>$value){
				    			?>
				    			<li>
				    			<a class = "magazine" id = "<?php echo $key;?>" href = "#">
				    			<?php echo $value['magname'];?>
				    			<i class = "icon-chevron-right icon-white pull-right"></i>
				    			</a>
				    			<div class = "subMagazine">
				    				<?php
				    				foreach($value['sites'] as $index=>$site){
				    					?>
				    					<p class = 'sources' id = '<?php echo $site['sites_id'];?>'><?php
				    						if($site['sites_name'] == "")
				    							echo $site['sites_url'];
				    						else
				    							echo $site['sites_name']; ?></p>
				    					<?php
				    				}  
				    				?>
				    			</div>
				    			</li>
				    			<?php 
				    		} 
				    	?>
				    	<li>
					    	<a class = "magazine" id = "favorites" href = "#">Favorites
					    		<i class = "icon-chevron-right icon-white pull-right"></i>
					    	</a>
					    	<div class = "subMagazine">
					    	</div>
				    	</li>
					</ul>
				</div>
			</div>
	    	<!--  End ofSide Pane Content-->
	    	<div class = "span9 mainContent" style = "margin-left:25%">
		    	<div class = "row-fluid" id = "body-loading">
		    		<div class = "span6 offset6">
		    			<img class = "pull-center" src = "images/ajax-loader.gif"/>
		    		</div>
		    		
		    	</div>
	    		<div class = "row-fluid" id = "feedsrow">
	    		</div>
	    		<div style = "margin-top:20px;margin-bottom:100px;">
		    		<button class = "btn btn-large btn-primary pull-left controls" id = "previous"><i class = "icon-backward"></i> Previous</button>
		    		<button class = "btn btn-primary btn-large pull-right controls" id = "next">Next <i class = "icon-forward"></i></button>
	    		<div class="lightbox fade" id="demoLightbox" style="display: none;">
	    			<div class = 'closeLightbox'>
		    			<img src = 'images/button_lightbox_close.png'></img>
		    		</div>
		    		
		    		<div class ='leftNav'>
		    			<img src = 'images/arrow-left.png' height= "110" width = "110"></img>
		    		</div>
		    		
		    		<div class = 'rightNav'>
		    			<img src = 'images/arrow-right.png' height= "110" width = "110"></img>
		    		</div>
					<div id='lightbox-content' style ="width:90%" class = 'pull-center'>
					
					<iframe id = "loadbox" class = 'pull-center' style = 'margin-left:-50px'>
					</iframe>
					
						<h1 class = "pull-center lightbox-elements" id = 'lightbox-title'>Title</h1>
						<h3 style = "text-decoration:underline;" class = "lightbox-elements pull-center" id = 'lightbox-source'>source</h3>
						<p id = 'lightbox-description' class = 'lightbox-elements'></p>
						<a href = "#" target = "_blank" id = "readmore" class ='lightbox-elements pull-left'>Read More...</a>
					</div>
				</div>
	    		
	    	</div>
	    </div>
	    </div>
    <!-- End of Main Content -->
  </body>
</html>
    