<header id='masthead'>
      <nav class='navbar navbar-fixed-top'>
        <div class='navbar-inner'>
          <div class='container'>
          
            <a class='btn btn-navbar' data-target='.nav-collapse' data-toggle='collapse'>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
            </a>
            <h1 class='brand' style = "padding-top:5px;padding-bottom:5px;margin-left:25px;">
              <a href='index.php' style = "position:relative;top:10px;"><img id = "gridfeed" style = 'position:relative;bottom:10px;'src = "images/gridfeeds.png" height = "70px" width = "70px"></img></a>
            </h1>
            <div class='nav-collapse'>
            <div class = "pull-right">
            <form class="navbar-search" style = "margin-right:200px" action = "results.php" method = "POST">
			  <input type="text" class="search-query" name = "searchquery" placeholder="Search for Users.."/>
			</form>
			<div class = "btn-group pull-right" style = "margin-right:50px">
            	<button class = "btn btn-inverse dropdown-toggle" data-toggle = "dropdown">
            	<i class = "icon-cog"></i>
            	</button>
            	<ul class = "dropdown-menu pull-center">
            		<li class = "dropdown-caret right">
				  		<span class = "caret-outer"></span>
				  		<span class = "caret-inner"></span>
			  		</li>
	            	<li><a href = "account.php">Account Settings</a></li>
	            	<li><a href = "scripts/logout.php" style= "padding-right:90px">Logout</a></li>
            	</ul>
            </div>
			</div>
            <ul class = "nav pull-left">
            <li style = 'margin-left:20px'><a class = 'navbar-hrefs' href = "reader.php">
            <i class = 'icon-dashboard'></i> Dashboard</a></li>
            <li><a class = 'navbar-hrefs' href = "manage.php"><i class = 'icon-pencil'>
            </i> Customize</a></li>
            <li class = "dropdown">
            <a class = 'navbar-hrefs' id = "drop1" href = "#" role = "button" class = "dropdown-toggle" data-toggle="dropdown"><i class = 'icon-group'></i> Social
      		</a>
      		<ul class = "dropdown-menu" role = "menu" arid-labelledby="drop1">
      			<li><a href = "friends.php">Following</a></li>
      			<li><a href = "profile.php">Profile</a></li>
      		</ul>
            </li>
            </ul>
            </div>
          </div>
        </div>
      </nav>
    </header>