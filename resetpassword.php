<?php
include 'scripts/connect.php';
	if(isset($_POST['hash']) && isset($_POST['password']) 
			&& isset($_POST['confirmPassword']) && isset($_POST['email'])){
		?>
		<?php include 'head.php';?>
		<title>Reset Password</title>
		</head>
		<body class='theme-pattern-lightmesh'>
		<!-- Page Header -->
		<header id='masthead'>
		<nav class='navbar navbar-fixed-top'>
		<div class='navbar-inner'>
		<div class='container'>
		<a class='btn btn-navbar' data-target='.nav-collapse' data-toggle='collapse'>
		<span class='icon-bar'></span>
		<span class='icon-bar'></span>
		<span class='icon-bar'></span>
		</a>
		<h1 class='brand' style = "padding-top:5px;padding-bottom:5px">
              <a href='index.php' style = "position:relative;top:10px;"><img style = 'position:relative;bottom:10px;'src = "images/gridfeeds.png" height = "70px" width = "70px"></img> &nbsp Grid<span style = 'color:#57b94a;'>Feeds</span></a>
            </h1>
		<div class='nav-collapse'>
		</div>
		</div>
		</div>
		</nav>
		</header>
		<?php 
		$password = $_POST['password'];
		$confirmpassword = $_POST['confirmPassword'];
		$email = $_POST['email'];
		$hash = $_POST['hash'];
		
		if($password!= "" && $confirmpassword!= "" && $password === $confirmpassword){
			
			
			$query = "UPDATE mashup_users SET user_password = ? WHERE user_email = ? AND user_password_reset_id = ? AND user_password_reset_status = 1";
			$hashedpassword = crypt($password,"$1$foreverdope12$");
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($hashedpassword,$email,$hash));
				?>
						<div id='content' role='main'>
			      <section class='section alt pull-center'>
			        <div class='container'>
			          <div>
			            <h1>You have successfully reset your password!</h1>
			          </div>
			        </div>
			      </section>
						<?php 		
						$query = "UPDATE mashup_users SET user_password_reset_status = 0 WHERE user_email = ?";
						$stmt = $pdo->prepare($query);
						$stmt->execute(array($email));
		}		
	}
	else if(isset($_GET['email']) && isset($_GET['hash'])){
		$email = $_GET['email'];
		$hash = $_GET['hash'];
		$query = "SELECT * FROM mashup_users WHERE user_email = ? AND user_password_reset_id = ? AND user_password_reset_status = 1";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($email,$hash));
		?>
		<?php include 'head.php';?>
		<title>Reset Password</title>
		</head>
		<body class='theme-pattern-lightmesh'>
		<!-- Page Header -->
		<header id='masthead'>
		<nav class='navbar navbar-fixed-top'>
		<div class='navbar-inner'>
		<div class='container'>
		<a class='btn btn-navbar' data-target='.nav-collapse' data-toggle='collapse'>
		<span class='icon-bar'></span>
		<span class='icon-bar'></span>
		<span class='icon-bar'></span>
		</a>
		<h1 class='brand' style = "padding-top:5px;padding-bottom:5px">
              <a href='index.php' style = "position:relative;top:10px;"><img style = 'position:relative;bottom:10px;'src = "images/gridfeeds.png" height = "70px" width = "70px"></img> &nbsp Grid<span style = 'color:#57b94a;'>Feeds</span></a>
            </h1>
		<div class='nav-collapse'>
		</div>
		</div>
		</div>
		</nav>
		</header>
		<?php 
		if($stmt->rowCount() == 1){
			$result = $stmt->fetch();
			$user_id = $result['user_id'];
?>
    <!-- Main Content -->
    <div id='content' role='main'>
      <section class='section alt pull-center'>
        <div class='container'>
          <div>
            <h1>Reset Your password</h1>
          </div>
        </div>
      </section>
      <section class = "section">
      <div class='container'>
          <div class = "row">
          	<div class = "span8 offset1">
          		<div class = "alert-danger">
          			<p></p>
          		</div>
          		<form id = "resetPassword" action = "resetpassword.php" method = "POST">
          			<label>Enter your new Password:</label>
          			<input type = "password" class = "span5" name = "password" placeholder="Password"/>
          			<label>Confirm your new Password:</label>
          			<input type = "password" class = "span5" name = "confirmPassword" placeholder="Password"/><br/>
          			<input type = "hidden" name = "hash" value = "<?php echo $hash?>"/>
          			<input type = "hidden" name = "email" value = "<?php echo $email?>"/>
          			<input type = "submit" class = "span3 btn btn-large btn-block btn-primary" value = "Submit"/>
          		</form>
          	</div>
          </div>
        </div>
      </section>
    </div>
    <?php
		}
		else{
			?>
			<div id='content' role='main'>
			<section class='section alt pull-center'>
			<div class='container'>
			<div>
			<h1>The link has expired</h1>
			</div>
			</div>
			</section>
			<?php 
		}
	}
	else{
		header("Location:404.html"); 
	}
    ?>
    <!-- Page Footer -->
    <footer class='section' id='footer' role='contentinfo'>
      <div class='container'>
        <div class='row-fluid'>
          <div class='span4'>
            <h3>Contact us</h3>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
            </p>
            <ul class='icons'>
              <li>
                <i class='icon-envelope'></i>
                <a href='mailto:dave@davegandy.com'>John Doe</a>
              </li>
              <li>
                <i class='icon-twitter'></i>
                <a href='http://twitter.com/oxygenna/' target='_blank'>@FortAweso_me</a>
              </li>
              <li>
                <i class='icon-facebook'></i>
                <a href='http://kyruus.com' target='_blank'>oxygenna</a>
              </li>
            </ul>
          </div>
          <div class='span4'>
            <h3>Recent news</h3>
            <ul class='icons'>
              <li>
                <p><i class='icon-twitter'></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  <small>
                    by
                    <a href='#'>oxygenna.com</a>
                  </small>
                </p>
              </li>
              <li>
                <p><i class='icon-twitter'></i>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                  <small>
                    by
                    <a href='#'>oxygenna.com</a>
                  </small>
                </p>
              </li>
            </ul>
          </div>
          <div class='span4'>
            <h3>Latest posts</h3>
            <ul class='unstyled'>
              <li>
                <h4>
                  <a href='#'>A responsive business theme</a>
                </h4>
                <p>
                  <small>November 12, 2012</small>
                </p>
              </li>
              <li>
                <h4>
                  <a href='#'>Designed for big and small screens</a>
                </h4>
                <p>
                  <small>August 23, 2012</small>
                </p>
              </li>
              <li>
                <h4>
                  <a href='#'>Based on bootstrap framework</a>
                </h4>
                <p>
                  <small>July 22, 2011</small>
                </p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  </body>
</html>
