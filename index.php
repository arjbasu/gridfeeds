<?php
	session_start();
	include 'readcookie.php';
	if((isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
		header("Location:234.php");
	} 
?>
<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset='utf-8' />
    <!-- %meta{:content => "IE=edge,chrome=1", "http-equiv" => "X-UA-Compatible"} -->
    <title>Gridfeeds</title>
    <meta content='Bootsrap based theme' name='description' />
    <!--[if lt IE 9]>
      <script src='http://html5shim.googlecode.com/svn/trunk/html5.js'></script>
    <![endif]-->
    <link href='images/favicon.ico' rel='shortcut icon' />
    <link href="stylesheets/bootstrap.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="stylesheets/font-awesome.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="stylesheets/responsive.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="stylesheets/theme.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="stylesheets/fonts.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" />
    <script type = "text/javascript" src = "js/jquery.js"></script>
    <script type = "text/javascript" src = "js/bootstrap.min.js"></script>
    <script type = "text/javascript" src = "js/custom.js"></script>
    <script language="javascript" src="js/jquery.tweet.js" type="text/javascript"></script>
    <script type='text/javascript'>
    jQuery(function($){
        $(".tweets").tweet({
            username: "gridfeeds",
            join_text: "auto",
            avatar_size: 0,
            count: 2,
            //auto_join_text_default: "we said,", 
            //auto_join_text_ed: "we",
            //auto_join_text_ing: "we were",
            //auto_join_text_reply: "we replied to",
            //auto_join_text_url: "we were checking out",
            loading_text: "loading tweets..."
        }).bind("empty", function() { $(this).append("No tweets found"); });;
    });
</script>
  </head>
  <body class='theme-pattern-lightmesh'>
    <!-- Page Header -->
    <header id='masthead'>
      <nav class='navbar navbar-fixed-top'>
        <div class='navbar-inner'>
          <div class='container'>
            <a class='btn btn-navbar' data-target='.nav-collapse' data-toggle='collapse'>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
            </a>
            <h1 class='brand' style = "padding-top:5px;padding-bottom:5px">
              <a href='index.php' style = "position:relative;top:10px;"><img style = 'position:relative;bottom:10px;'src = "images/gridfeeds.png" height = "70px" width = "70px"></img> &nbsp Grid<span style = 'color:#57b94a;'>Feeds</span></a>
            </h1>
            <div class='nav-collapse'>
            <ul class = "nav pull-right">            	
            	<a class='btn ' id = "signupButton" href = "#">Signup</a>
               <a class='btn btn-primary' id = "loginButton" href = "#">Login</a>
               </ul>
               
            </div>
          </div>
        </div>
      </nav>
    </header>
    
    <!-- Login/Signup Modal -->
    	
   	<div class="modal hide fade" id = "loginmodal">
		<div class="modal-header">
	    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    	<h3 class = "pull-center login">Login</h3>
	    	<h3 class = "pull-center signup">Create an Account</h3>
	    	<h3 class = "pull-center resetPassword" style = "display:none;">Reset your password</h3>
	  	</div>
	  	
	  	<div class="modal-body">
	  		<div class = "pull-center" id = "loading" style = "display:none">
	  			<img src = "images/ajax-loader.gif"></img>
	  		</div>
	  		<div class = "pull-center" id = "successdiv" style = "display:none">
	  			<h3 class = "pull-center">Congratulations!!</h3>
	  			<p> You have succesfully registered for GridFeeds. A confirmation email, with further 
	  			instructions has been sent to <span id = "maillink"></span></p>
	  		</div>
	  		<div class = "alert alert-danger" id = "errordiv" style = "display:none">
	  			<p class = "pull-center"></p>
	  		</div>
	  		<div class = "pull-center resetPassword" style = "display:none">
	  			<h3 class = "pull-center">Enter your email</h3>
	  			<form id = "resetPasswordForm">
	  				<input class = "span5" type = "text" name = "email" id = "email" placeholder = "Your email"/>
	  			</form>
	  		</div>
	  		<div id = "resetPassworddiv" class = "alert" style = "display:none">
	  			<p class = "pull-center" id = "resetPasswordStatus"></p>
	  		</div>
	  		<div class="pull-center">
	  		<form id = "signupForm" class = "signup">
  			<input class = "span5" type = "text" id = "Name" name = "name" placeholder = "Your Name"/><br/>
			<input type = "text" class = "span5" id = "email" name = "email" placeholder = "Your Email"/>
			<input type = "password" class = "span5" id = "password" name = "password" placeholder = "Password"/>
			</form>
			</div>
	  	
	    	<form id = "loginForm" class = "login">
	    	<div class="input-prepend pull-center">
  			<span class="add-on"><i class = "icon-user"></i></span>
  			<input class = "span4" type = "text" id = "email" name = "email" placeholder = "Email"/><br/>
			</div>
			<div class = "input-prepend pull-center">
				<span class="add-on"><i class = "icon-lock"></i></span>
  			<input type = "password" class = "span4" id = "password" name = "password" placeholder = "Password"/>
			</div>
			<div class = "span4" style = "position:relative;left:50px;">
			<label class="checkbox">
      			<input type="checkbox" name = "rememberme" value = "remember"> Remember Me
    		</label>
    		</div>
	    	</form>	
	  	</div>
	  	
	  	<div class="modal-footer">
		  	<a href="#" class="btn pull-left signup" id = "showLogin">Already a Member?</a>
		  	<a href="#" class="btn btn-primary pull-right signup" id = "signupProceed">Signup!</a>
		  	
	    	<a href="#" class="btn pull-left login" id = "forgotPassword">Forgot Password?</a>
	    	<a href="#" class="btn btn-primary pull-right login" id = "loginProceed">Login</a>
	    	<a href="#" class="btn btn-primary pull-right resetPassword" id = "resetPasswordButton" style = "display:none">Next</a>
	  	</div>
	</div>
    <!-- End of Login/Signup Modal -->
    
    
    <!-- Main Content -->
    <div id='content' role='main'>
      <section class='section alt' id='promo'>
  <div class='container'>
      <!-- Carousel items -->
          <div class='row-fluid'>
            <div class='span6 offset1'>
              <img alt='imac' class='pull-right' src='images/item.png' />
            </div>
            
            <div class='span5'>
						<h1 style="margin-top: 130px; font-size: 32px">
							Tired of <span style="color: #76c2d9">Surfing</span> the Web?
						</h1>
						<p style="width: 420px; font-size: 20px;margin-left:5px">
							<span style="color: #57b94a">So are we</span>.....Bring all of
							your entertainment into one place, organize it all into
							magazines, and enjoy.
						</p>
					</div>
      </div>
</section>
<section class='section'>
  <div class='container'>
    <div class='page-header'>
      <h1>
        What can you do?
      </h1>
    </div>
    <div class='row-fluid'>
      <ul class='thumbnails bordered pull-center'>
        <li class='span3'>
          <h3>
            <i class='icon-rss huge-icon'></i>
            <span class='blocked'>RSS</span>
          </h3>
          <p>
            Add all of your favorite websites & organize them into magazines that you create! 
          </p>
        </li>
        <li class='span3'>
          <h3>
            <i class='icon-dashboard huge-icon'></i>
            <span class='blocked'>Dashboard</span>
          </h3>
          <p>
            Browsing the web to catch up on the "latest"? View all of your websites in one convenient grid
          </p>
        </li>
        <li class='span3'>
          <h3>
            <i class='icon-star huge-icon'></i>
            <span class='blocked'>Favorites</span>
          </h3>
          <p>
            Love it? Ran out of time? Save it for later, it'll be waiting in your favorites magazine.
          </p>
        </li>
        <li class='span3'>
          <h3>
            <i class='icon-group huge-icon'></i>
            <span class='blocked'>Social</span>
          </h3>
          <p>
            Share your magazines! View other's profiles to see what they're reading about & find new sources!
          </p>
        </li>
      </ul>
    </div>
  </div>
</section>
    </div>
    <?php include 'termsofagreement.php';
    	include 'privacypolicies.php';
    ?>
    <!-- Page Footer -->
    <footer class='section alt' role='contentinfo'>
      <div class='container'>
        <div class='row-fluid'>
          <div class='span4'>
            <h3>Contact us</h3>
            <p>
              Feed free to get in touch! We're always excited to hear from our user
              regarding anything from feedback, to just saying what's up! Please allow 24-48 hours
              to receive a reply. Cheers!
            </p>
            <ul class='icons'>
              <li>
                <i class='icon-envelope'></i>
                <a href='mailto:info@gridfeeds.com'>GridFeeds</a>
              </li>
              <li>
                <i class='icon-twitter'></i>
                <a href='http://twitter.com/gridfeeds' target='_blank'>@GridFeeds</a>
              </li>
              <li>
                <i class='icon-facebook'></i>
                <a href='http://www.facebook.com/gridfeeds' target='_blank'>GridFeeds</a>
              </li>
            </ul>
          </div>
          <div class='span4'>
            <h3>Recent news</h3>
            <div class = "tweets">
            	<ul class='icons'>
              <li>
                <p><i class='icon-twitter'></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  <small>
                    by
                    <a href='#'>oxygenna.com</a>
                  </small>
                </p>
              </li>
              <li>
                <p><i class='icon-twitter'></i>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                  <small>
                    by
                    <a href='#'>oxygenna.com</a>
                  </small>
                </p>
              </li>
            </ul>
            </div>
          </div>
          <div class='span4'>
            <h3>References</h3>
            <ul class='unstyled'>
              <li>
                <h4>
                  <a href='#privacypolicy' data-toggle="modal">Privacy</a>
                </h4>
                <p>
                  <small>View our privacy policies</small>
                </p>
              </li>
              <li>
                <h4>
                  <a href='#termsofagreement' data-toggle="modal">Terms of Agreement</a>
                </h4>
                <p>
                  <small>View our Terms of Agreement</small>
                </p>
              </li>
              <li>
                <h4>
                  <a href='mailto:careers@gridfeeds.com'>We're Hiring</a>
                </h4>
                <p>
                  <small>Work with us</small>
                </p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  </body>
</html>
