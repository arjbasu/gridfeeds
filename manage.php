<?php
session_start();
include 'readcookie.php';
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	header("Location:index.php");
}
else{
	include 'scripts/connect.php';
	$user_id = $_SESSION['user_id'];
	$tablename = "mashup_magazines_$user_id";
	$query = "SELECT * FROM $tablename";
	$result = mysql_query($query);
	if(!$result){
		die("Unable to query databases");
	}
	else{
		if(mysql_num_rows($result) == 0){
			header("Location:234.php");
		}
		else{
			$magazines = array();
			while($temp = mysql_fetch_assoc($result)){
				$temparray = array();
				$magname = $temp['magazines_name'];
				$magid = $temp['magazines_id'];
				$temparray['magname'] = $magname;
				$temparray['sites'] = array();
				$magazines[$magid] = $temparray;
			}
			$query = "SELECT * FROM mashup_sites_$user_id";
			$result = mysql_query($query);
			if(!$result){
				die("Unable to query database");
			}
			else{
				while($temp = mysql_fetch_assoc($result)){
					$magazineid = $temp['sites_magazine_id'];
					array_push($magazines[$magazineid]['sites'], $temp);
				}
				include 'head.php';
				?>
				<title>Customize</title>
			    <script type = "text/javascript" src = "js/jquery-ui-1.9.1.custom.min.js"></script>
			    <script type = "text/javascript" src = "js/jquery.mousewheel.min.js"></script>
			    <script type = "text/javascript" src = "js/jquery.mCustomScrollbar.js"></script>
				<script type = "text/javascript" src = "js/manage.js"></script>
				<script type="text/javascript" src="js/jquery.loadmask.min.js"></script>
				<link href="css/jquery.loadmask.css" rel="stylesheet" type="text/css" />
				<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
			    <style>
			    	#dragSelection{
			    		position:absolute;
			    		display:none;
			    		cursor:move;
			    	}
			    </style>
			    </head>
			    
			    <body class='theme-pattern-lightmesh'>
			    <?php include 'header.php';?>
			    
			    <div class = "container">
	    			<div class = "row">
				   		<section id = "promo" class = "section alt">
				    		<div class = "addSection">	
				    			<div class="dropdown pull-right">
				    			<a href = "#" class = "dropdown-toggle btn btn-info" data-toggle = "dropdown" id = "siteadd">
				    			Add New Site <i class = "icon-plus"></i>
				    			</a>
				            	<div class="dropdown-menu pull-right" style="padding: 15px; padding-bottom: 0px; background-color:white;padding-right:5px;">
				            	<span class = "dropdown-caret right">
							  		<span class = "caret-outer"></span>
							  		<span class = "caret-inner"></span>
							  	</span>
				              	<!-- Login form here -->
				              	<form class = "form-inline span4" style = "background-color:white;margin:-8px -5px 16px 24px" id = "siteForm">
				              		<label>Enter a new site</label>
							    	<input type = "text" class = "span3" placeholder = "Site URL" name = "siteUrl" id = "siteUrl"/>
							    	<button type = "submit" class = "btn btn-blue">Add</button>
							    	<img id = "siteloading" src = "images/ajax-loader.gif" class = "pull-center" style = "display:none;">
							    	</img>
							    </form>
							    		
							    
							    
				            	</div>
				    		</div>
				    		
				    		<div class="dropdown pull-right">
				    			<a href = "#" class = "dropdown-toggle btn" data-toggle = "dropdown" style = "position:relative;left:-5px" id = "magadd">
				    			Add New Magazine <i class = "icon-plus"></i>
				    			</a>
				            	<div class="dropdown-menu pull-right" style="padding: 15px; padding-bottom: 0px; background-color:white;padding-right:5px;">
				            	<span class = "dropdown-caret right">
							  		<span class = "caret-outer"></span>
							  		<span class = "caret-inner"></span>
							  	</span>
				              	<!-- Login form here -->
				              	<form class = "form-inline span4" style = "background-color:white;margin:-8px -5px 16px 24px" id = "magazineForm">
				              		<label>Create a new Magazine</label>
							    	<input type = "text" class = "span3" placeholder = "Magazine Name" name = "magazinename" id = "magazineName"/>
							    	<button type = "submit" class = "btn btn-blue">Add</button>
							    	<img src = "images/ajax-loader.gif" id = "magloading" class = "pull-center" style = "display:none"></img>
							    </form>
							    	
							    
				            	</div>
				    		</div>
				    		
							</div>
			    		</section>
			    		<div class = "span12" id = "magazinelist" style = "margin-top:40px">
			    			<div class = "row">
			    				<div class = "span12 row alert alert-danger" id = "errordiv" style = "display:none;">
			    					<p class = "pull-center"></p>
			    				</div>
			    				
			    				<div class = "span12 row alert alert-success" id = "successdiv" style = "display:none;">
			    					<p class = "pull-center"></p>
			    				</div>
			    			</div>
			    			<div class = "row">
			    			<?php
			    				$i = 0;
			    				foreach($magazines as $id=>$magazine){
			    					?>
			    					<div class = "span3 magazinecell">
			    						<h2 class = "pull-center magtitle"><span class = 'mainmagname'><?php echo $magazine['magname'];?></span></h2>
			    						<div class = "scrollable droppable" 
			    						id = "mag<?php echo $id;?>">
			    							<div class = "text">
			    							<?php
			    								$sites = array();
			    								$sites = $magazine['sites'];
			    								$length = count($magazine['sites']);
			    								for($j = 0; $j < $length; $j++){
			    									?>
			    									<p class = "draggable" id = "site<?php echo $sites[$j]['sites_id'];?>">
			    									<?php echo $sites[$j]['sites_name'];?></p>
			    									<?php 
			    								} 
			    							?>
			    							</div>
			    						</div>	
			    					</div>
			    					<?php
			    					if($i!=0 && ($i-3)%4 ==0){
			    						?>
			    						</div>
			    						<div class = "row">
			    						<?php 
			    					}
			    					$i++;
			    				}
			    				if(($i-1)%3!=0){
			    					echo "</div>";
			    				}
			    			?>
			    		</div>
			    		<div id = "dragSelection">
    					</div>			    		
	    			</div>
    			</div>
    			<div class = "modal hide fade" id = "magazineeditmodal">
    				<div class = "modal-header">
    					<h2 class = "pull-center">
    						Edit Magazine Name
    					</h2>
    				</div>
    				<div class = "modal-body">
    				<div class = "error alert alert-danger" style = "display:none">
						  	<p class = "pull-center"></p>
						  </div>
						  <div class = "success alert alert-success" style = "display:none">
						  	<p class = "pull-center"></p>
						  </div>
    					<form id = "mageditform" class = "pull-center">
    						<input type = "text" class = "span5" name = "magazine_name" id = "mageditname" />
    						<input type = "hidden" name = "magazine_id" id = "mageditid" value = ""/>
    					</form>
    				</div>
    				<div class = "modal-footer">
    					<button class = "btn btn-primary pull-right" id = "rename" style = "margin-left:5px">Rename</button>
    					<a href = "#magazineeditmodal" data-dismiss = "modal" class = "btn pull-right">Close</a>
    				</div>
    			</div>
    			
    			<div class="modal hide fade" id = "magazineSelect">
						  <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						    <h2 class = "pull-center">Select a Magazine</h2>
						  </div>
						  <div class="modal-body">
						  <div class = "error alert alert-danger" style = "display:none">
						  	<p class = "pull-center"></p>
						  </div>
						  <div class = "success alert alert-success" style = "display:none">
						  	<p class = "pull-center"></p>
						  </div>
						    <div id = "loading" style = "display:none;">
						    	<img src = "images/ajax-loader.gif" class = "pull-center"></img>
						    </div>
						    <div id = "magazinelisting">
						    	<form id = "magazinelistForm">
						    		<?php foreach($magazines as $id=>$value){
						    		?>
						    		<button type = "button" class = "btn btn-primary formmagazinename" 
						    		id = "mag<?php echo $id;?>" style = "margin:5px">
						    		<?php echo $value['magname'];?></button>
						    		<input type = 'hidden' name = 'siteurl' id = 'formsiteurl' value = ""/>
						    		<?php }?>	
						    	</form>
						    </div>
						   </div>
						   <div class = "modal-footer">
						   	<a href = "#magazineSelect" data-dismiss="modal" class = "pull-right btn">Close</a>
						   </div>
					</div>
				<?php 
				
			}
			
		}
	}
}
?>
</body>
</html>