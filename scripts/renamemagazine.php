<?php
session_start();
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	$status = "unauthorized";
	$message = "Login First!!";
}
else if(isset($_POST['magazine_id']) && isset($_POST['magazine_name'])){
	include 'connect.php';
	$magid = $_POST['magazine_id'];
	$magname = $_POST['magazine_name'];
	$userid = $_SESSION['user_id'];
	$tablename = "mashup_magazines_$userid";
	$query = "UPDATE $tablename SET magazines_name = ? WHERE magazines_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($magname,$magid));
	if($stmt->rowCount() == 1){
		$status = "success";
		$message = "Magazine Succesfully renamed";
	}	
	else{
		$status = "error";
		$message = "Error renaming magazine";
	}
}

else{
	$status = "error";
	$message = "Improper parameters passed";
}
include 'print.php';