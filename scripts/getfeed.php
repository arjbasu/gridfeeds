<?php
session_start();
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	$status = "Error";
	$message = "unauthenticated";
}
date_default_timezone_set('Asia/Calcutta');
$url = $_GET['url'];
if(!isset($_GET['url'])){
	$status = "Error";
	$message = "Improper parameters passed";
}
else{
	$user_id = $_SESSION['user_id'];
	include 'connect.php';
	require_once('../php/autoloader.php');
	include 'domainlist.php';
	require_once '../simplehtmldom/simple_html_dom.php';
	$ids = "(";
	foreach($_GET['url'] as $key=>$url){
		$ids = $ids.$url.",";
	}
	$ids = substr($ids,0,-1);
	$ids = $ids.")";
	$query = "SELECT * FROM mashup_sites_$user_id WHERE sites_id IN $ids";
	$result = mysql_query($query);
	if(!$result){
		$status = "error";
		$message = "Unable to query databases";
	}
	else{
		$urls = array();
		while($temp = mysql_fetch_assoc($result)){
			$url = $temp['sites_feed_url'];
			if(strpos($url,"http://")===false){
				$url = $temp['sites_url'].$url;
			}
			array_push($urls,$url);
		}
		
		$feed = new SimplePie();
		$feed->set_feed_url($urls);
		
		// Run SimplePie.
		$feed->init();
		
		// This makes sure that the content is sent to the browser as text/html and the UTF-8 character set (since we didn't change it).
		$feed->handle_content_type();
		$items = array();
		$itemeach = array();
		foreach ($feed->get_items() as $item){
			$itemeach['link'] = $item->get_permalink();
			$itemeach['title'] = $item->get_title();
			$itemeach['description'] = $item->get_content();
			$itemeach['date'] = $item->get_date('j F Y | g:i a');
			$parent = $item->get_feed();
			$itemeach['source'] = str_replace($domains,"",$parent->get_title());
			$desc_dom = str_get_html($itemeach['description']);
			if(strpos($itemeach['description'],"<img") === false){
				$itemeach['thumbnail'] = "";
			}
			else{
				$element = $desc_dom->find('img',0);
				$link = $element->src;
				$itemeach['thumbnail'] = $link;
			}
			$itemeach['caption'] = $desc_dom->plaintext;
			array_push($items, $itemeach);
		}
		$message = $items;
		$status = "Success";
	}	
}
$response['status'] = $status;
$response['message'] = $message;
$response['url'] = $url;
echo json_encode($response);

?>