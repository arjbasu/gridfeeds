<header id='masthead'>
      <nav class='navbar navbar-fixed-top'>
        <div class='navbar-inner'>
          <div class='container'>
            <a class='btn btn-navbar' data-target='.nav-collapse' data-toggle='collapse'>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
              <span class='icon-bar'></span>
            </a>
            <div class='nav-collapse'>
            <div class = "pull-right">
            <form class="navbar-search" style = "margin-right:100px">
			  <input type="text" class="search-query" placeholder="Search"/>
			</form>
			<div class = "btn-group pull-right" style = "margin-right:50px">
            	<button class = "btn btn-inverse dropdown-toggle" data-toggle = "dropdown">
            	<i class = "icon-cog"></i>
            	</button>
            	<ul class = "dropdown-menu pull-center">
            		<li class = "dropdown-caret right">
				  		<span class = "caret-outer"></span>
				  		<span class = "caret-inner"></span>
			  		</li>
	            	<li><a href = "account.php">Settings</a></li>
	            	<li><a href = "scripts/logout.php">Logout</a></li>
            	</ul>
            </div>
			</div>
            <ul class = "nav pull-left">
            <li style = 'margin-left:230px'><a href = "reader.php"><i class = 'icon-dashboard'></i> Dashboard</a></li>
            <li><a href = "manage.php"><i class = 'icon-pencil'></i> Customize</a></li>
            <li><a href = "profile.php"><i class = 'icon-dashboard'></i> Profile</a></li>
            </ul>
            </div>
          </div>
        </div>
      </nav>
    </header>