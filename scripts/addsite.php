<?php
session_start();
	if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
		$status = "unauthorized";
		$message = "Login First!!";
	}
	else if(isset($_POST['siteurl']) && isset($_POST['magazine_id'])){
		include 'connect.php';
		include 'domainlist.php';
		include '../phpquery/phpQuery.php';
		require_once('../php/autoloader.php');
		$user_id = $_SESSION['user_id'];
		$magid = $_POST['magazine_id'];
		foreach($_POST['siteurl'] as $key => $url){
			$sitename = "";
			if($url != ""){
				if(strpos($url,"http://") === false)
					$url = "http://".$url;
				$query = "SELECT * FROM mashup_sites_$user_id WHERE sites_url = ?";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($url));
				if($stmt->rowCount() >= 1){
					$status = "error";
					$message = "Already Exists";
				}
				else{
					if (preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $url)) {
						$feed = new SimplePie();
						$feed->set_feed_url($url);
						if(!$feed->error()){
							$feed->init();
							$sitename = $feed->get_title();
							if($sitename == ""){
								$status = "error";
								$message = "Invalid Link";
								break;
							}
							else{
								$sitename = str_replace($domains,"",$feed->get_title());
								$tablename = "mashup_sites_$user_id";
								$query = "SELECT * FROM $tablename WHERE sites_name = '$sitename'";
								$result = mysql_query($query);
								if(mysql_num_rows($result) >=1){
									$status = "error";
									$message = "Already Exists";
									break;
								}
								else{
									$query = "INSERT into $tablename(sites_magazine_id,sites_url,sites_feed_url,sites_name) VALUES(?,?,?,?)";
									//echo $query;
									$stmt = $pdo->prepare($query);
									$stmt->execute(array($magid,$url,$url,$sitename));
									if($stmt->rowCount() == 1){
										$query = "UPDATE mashup_magazines_$user_id SET magazines_site_count=magazines_site_count+1 WHERE magazines_id = '$magid'";
										$result = mysql_query($query);
										if(!$result){
											$status = "error";
											$message = "Unable to update database";
											break;
										}
										else{
											$status = "Success";
											$message = "Site(s) Successfully added";
										}
									}
									else{
										$status = "error";
										$message = "Error adding site: $url, $magid, $sitename";
										break;
									}
										
								}
							}
							
						}
						else{
							$ch = curl_init();
							$timeout = 20;
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
							$data = curl_exec($ch);
							curl_close($ch);
							if(!$data){
								$status = "error";
								$message = "Unable to open URL";
								break;
							}
							else{
								$doc = phpQuery::newDocumentHTML($data);
								phpQuery::selectDocument($doc);
								$flag = 0;
								foreach(pq('link') as $link) {
									$relattribute =  $link->getAttribute("rel");
									if($relattribute == "alternate"){
										$feedurl = $link->getAttribute("href");
										$flag = 1;
										break;
									}
					
								}
								if($flag ==0){
									$status = "error";
									$message = "Site does not provide RSS feeds";
									break;
								}
								else{
									$sitename = pq("title")->text();
									$tablename = "mashup_sites_$user_id";
									$query = "SELECT * FROM $tablename WHERE sites_name = '$sitename'";
									$result = mysql_query($query);
									if(mysql_num_rows($result) >=1){
										$status = "error";
										$message = "Already Exists";
										break;
									}
									else{
										$query = "INSERT into $tablename(sites_magazine_id,sites_url,sites_feed_url,sites_name) VALUES(?,?,?,?)";
										//echo $query;
										$stmt = $pdo->prepare($query);
										$stmt->execute(array($magid,$url,$url,$sitename));
										if($stmt->rowCount() == 1){
											$query = "UPDATE mashup_magazines_$user_id SET magazines_site_count=magazines_site_count+1 WHERE magazines_id = '$magid'";
											$result = mysql_query($query);
											if(!$result){
												$status = "error";
												$message = "Unable to update database";
											}
											else{
												$status = "Success";
												$message = "Site(s) Successfully added";
											}
										}
										else{
											$status = "error";
											$message = "Error adding site: $url, $magid, $sitename";
											break;
										}
											
									}
					
								}
							}
						}
					}
					else
					{
						$status = "error";
						$message = "Invalid URL";
						break;
					
					}	
				}	
			}
			else{
				$status = "error";
				$message = "URL cannot be blank";
			}
				
		}
		
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
		
	}
	
	$response['status'] = $status;
	$response['message'] = $message;
	$response['siteurl'] = $_POST['siteurl'];
	echo json_encode($response);
?>