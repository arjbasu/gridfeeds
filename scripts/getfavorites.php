<?php
session_start();
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	$status = "Error";
	$message = "unauthenticated";
}
else{
	include 'connect.php';
	require_once '../simplehtmldom/simple_html_dom.php';
	$userid = $_SESSION['user_id'];
	$query = "SELECT * FROM mashup_favorites_$userid ORDER BY favorites_timeofadding DESC";
	$result = mysql_query($query);
	if(!$result){
		$status = "Error";
		$message = "Unable to query";
	}
	else{
		if(mysql_num_rows($result) == 0){
			$status = "Zeroresults";
			$message = "No results found";
		}
		else{
			$feeds = array();
			while($temp = mysql_fetch_assoc($result)){
				$feed = array();
				$feed['title'] = stripslashes($temp['favorites_title']);
				$feed['date'] = $temp['favorites_date'];
				$feed['link'] = $temp['favorites_link'];
				$feed['description'] = stripslashes($temp['favorites_description']);
				$feed['id'] = $temp['favorites_id'];
				$desc_dom = str_get_html($feed['description']);
				$feed['caption'] = $desc_dom->plaintext;
				$feed['thumbnail'] = $temp['favorites_image'];
				$feed['source'] = $temp['favorites_source'];
				array_push($feeds,$feed);
			}
			$status = "Success";
			$message = $feeds;
		}
	}
}
$response['status'] = $status;
$response['message'] = $message;
echo json_encode($response);
?>