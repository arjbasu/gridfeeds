<?php
if ($_FILES["file"]["error"] > 0)
{
	$status = "Error";
	$message =   $_FILES["file"]["error"];
}
else
{
	if (file_exists("../images/" . $_FILES["file"]["name"]))
	{
		//echo $_FILES["file"]["name"] . " already exists. ";
		$status = "Success";
		$msg = $_FILES['file']['name'];
	}
	else
	{
		move_uploaded_file($_FILES["file"]["tmp_name"],
				"../images/" . $_FILES["file"]["name"]);
		//echo "Stored in: " . "images/" . $_FILES["file"]["name"];
		$status = "Success";
		$msg = $_FILES['file']['name'];
	}
	$response['status'] = $status;
	$response['message'] = $msg;
	echo json_encode($response);
}
?>