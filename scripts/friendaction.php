<?php
session_start();
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	$status = "unauthorized";
	$message = "Login First!!";
}
else if(isset($_POST['friend_id']) && isset($_POST['action'])){
	include 'connect.php';
	$id = $_POST['friend_id'];
	$action = $_POST['action'];
	$userid = $_SESSION['user_id'];
	if($action == "add"){
		$query = "SELECT * FROM mashup_friends_$userid WHERE friends_user_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($id));
		if($stmt->rowCount() >= 1){
			$status = "error";
			$message = "Already Friends";
		}
		else{
			$query = "INSERT INTO mashup_friends_$userid(friends_user_id,friends_status) VALUES(?,'confirmed')";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($id));
			if($stmt->rowCount() == 1){
				$status = "success";
				$message = "Friend Successfully added";
			}
			else{
				$status = "error";
				$message = "Unable to insert into databases";
			}	
		}
	}
	else if($action == "delete"){
		$query = "DELETE FROM mashup_friends_$userid WHERE friends_user_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($id));
		if($stmt->rowCount()==1){
			$status = "success";
			$message = "Friend Successfully deleted";
		}
		else{
			$status = "error";
			$message = "Unable to insert into databases";
		}
	}
}
else{
	$status = "error";
	$message = "Improper parameters passed";
}
include 'print.php';
?>