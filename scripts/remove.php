<?php
session_start();
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	$status = "unauthorized";
	$message = "Login First!!";
}
else if(isset($_POST['id']) && isset($_POST['type'])){
	include 'connect.php';
	$id = $_POST['id'];
	$type = $_POST['type'];
	$userid = $_SESSION['user_id'];
	$tablename = "mashup_".$type."_$userid";
	if($type == "sites"){
		$query = "SELECT * FROM $tablename WHERE $type"."_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($id));
		if($stmt->rowCount() == 1){
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			$magid = $temp['sites_magazine_id'];
		}
		else{
			$status = "error";
			$message = "Unable to fetch from database";
			$response['status'] = $status;
			$response['message'] = $message;
			die(json_encode($response));
		}
	}
	$query = "DELETE FROM $tablename WHERE $type"."_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($id));
	if($stmt->rowCount() == 1){
		if($type == "sites"){
			$query = "UPDATE mashup_magazines_$userid SET magazines_site_count = magazines_site_count-1 WHERE magazines_id = '$magid'";
			$result = mysql_query($query);
			if(!$result){
				$status = "error";
				$message = "Unable to interact with database";
			}
			else{
				$status = "Success";
				$message = "Successfully Deleted";
			}
		}
		else{
			$query = "DELETE FROM mashup_sites_$userid WHERE sites_magazine_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($id));
			if($stmt->rowCount() >= 0){
				$status = "Success";
				$message = "Successfully Deleted";
			}
			else{
				$status = "Error";
				$message = "Unable to delete sources";
			}
			
		}
		
	}
	else{
		$status = "error";
		$message = "Unable to delete";
	}
}
else{
	$status = "error";
	$message = "Improper Parameters passed";
}
include 'print.php';
?>