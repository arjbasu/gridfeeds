<?php
session_start();
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	$status = "Error";
	$message = "Unauthenticated";
}
else if(isset($_POST['id'])){
	include 'connect.php';
	$userid = $_SESSION['user_id'];
	$id = $_POST['id'];
	$query = "DELETE FROM mashup_favorites_$userid WHERE favorites_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($id));
	if($stmt->rowCount() != 1){
		$status = "Error";
		$message = "Database deletion error";
	}
	else{
		$status = "Success";
		$message = "Successfully removed";
	}
}
else{
	$status = "Error";
	$message = "Improper parameters passed";
}
$response['status'] = $status;
$response['message'] = $message;
echo json_encode($response);
?>