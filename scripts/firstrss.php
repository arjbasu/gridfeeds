<?php
function cmp($a, $b)
{
	if ($a['date'] == $b['date']) {
		return 0;
	}
	return ($a['date'] < $b['date']) ? -1 : 1;
} 
// Make sure SimplePie is included. You may need to change this to match the location of autoloader.php
// For 1.0-1.2:
#require_once('../simplepie.inc');
// For 1.3+:
require_once('php/autoloader.php');
 
// We'll process this feed with all of the default options.
$feed = new SimplePie();
 
// Set which feed to process.
$url1 = "http://www.espncricinfo.com";
$url2 = "http://www.espncricinfo.com/rss/content/story/feeds/0.xml";
$url3 = "http://sports-ak.espn.go.com/espn/rss/news";
$url4 = "http://www.androidpolice.com";
$url = array($url1,$url2,$url3,$url4);
$feed->set_feed_url($url);
 
// Run SimplePie.
$feed->init();
 
// This makes sure that the content is sent to the browser as text/html and the UTF-8 character set (since we didn't change it).
$feed->handle_content_type();
 
// Let's begin our XHTML webpage code.  The DOCTYPE is supposed to be the very first thing, so we'll keep it on the same line as the closing-PHP tag.
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>Sample SimplePie Page</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
</head>
<body>
 
	<div class="header">
		<h1><a href="<?php echo $feed->get_permalink(); ?>"><?php echo $feed->get_title(); ?></a></h1>
		<p><?php echo $feed->get_description(); ?></p>
	</div>
 
	<?php
	/*
	Here, we'll loop through all of the items in the feed, and $item represents the current item in the loop.
	*/
	$items = array();
	$itemeach = array();
	foreach ($feed->get_items() as $item){
		$itemeach['link'] = $item->get_permalink();
		$itemeach['title'] = $item->get_title();
		$itemeach['description'] = $item->get_description();
		$itemeach['date'] = $item->get_date('j F Y | g:i a');
		array_push($items, $itemeach);
	}
	
	//usort($items, 'cmp');
	
	?>
	<?php
		foreach($items as $key=>$value){
			?>
			<div class="item">
			<h2><a href="<?php echo $value['link']; ?>"><?php echo $value['title']; ?></a></h2>
			<p><?php echo $value['description']; ?></p>
			<p><small>Posted on <?php echo $value['date']; ?></small></p>
	</div>	
		<?php 
		} 
	?>
 
</body>
</html>