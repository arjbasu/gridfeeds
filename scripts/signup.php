<?php
	include 'connect.php';
	session_start();
	if(isset($_POST['name'])){
		$name = $_POST['name'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		if($name == "" || $email == "" || $password == ""){
			$status = "Failed";
			$response = "Some fields empty";
		}
		else{
			$query = "SELECT * FROM mashup_users WHERE user_email = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($email));
			if($stmt->rowCount()>=1){
				$status = "Error";
				$message = "Email already exists.";
			}
			else{
				$str = "$email$name$password";
				$hashvalue = md5($str);
				$stmt = $pdo->prepare("INSERT INTO mashup_users(user_name,user_email,user_password,user_verification_id)
						VALUES(?,?,?,?)");
				$hashedpassword = crypt($password,'$1$foreverdope12$');		
				$result = $stmt->execute(array($name,$email,$hashedpassword,$hashvalue));
				if($stmt->rowCount() == 1){
					$status = "Success";
					$response = "$email";
					$subject = "[GridFeeds] Verification of account for $name";
					$message = "Congatulations!! You have succesfully registered for GridFeeds.\n\n".
							"Please click on the link below to verify your account:\n\n".
							"http://www.foreverdope.com/mashup/scripts/verify.php?name=".urlencode($name)."&hash=$hashvalue\n\n".
							"If you did not register with GridFeeds, please ignore this email";
					$from = "no-reply@gridfeeds.com";
					$headers = "From:" . $from;
					mail($email, $subject, $message,$headers);
				
				}
				else{
					$status = "Error";
					$response = "Unable to insert into DB";
				}	
			}
			
		}
		$message = array();
		$message['status'] = $status;
		$message['response'] = $response;
		echo json_encode($message);
	}
	else{
		$status = "Error";
		$response = "Improper Params";
		$message = array();
		$message['status'] = $status;
		$message['response'] = $response;
		echo json_encode($message);
	}
?>