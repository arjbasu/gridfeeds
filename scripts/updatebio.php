<?php
session_start();
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	$status = "unauthorized";
	$message = "Login First!!";
}
else if(isset($_POST['image']) && isset($_POST['bio'])){
	include 'connect.php';
	$image = $_POST['image'];
	$bio = $_POST['bio'];
	if($bio == "" || strlen($bio)>140){
		$status = "Error";
		$message = "Please add a proper bio within 140 characters";
	}
	else{
		$user_id = $_SESSION['user_id'];
		$query = "UPDATE mashup_users SET user_dp = ?, user_bio = ? WHERE user_id = ?";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($image,$bio,$user_id));
		$status = "Success";
		$message = "Profile Picture and Bio successfully updated";
	}
}
else{
	$status = "Error";
	$message = "Improper Parameters passed";
}

$response['status'] = $status;
$response['message'] = $message;
echo json_encode($response);
?>