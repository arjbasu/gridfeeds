<?php
session_start();
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	$status = "unauthorized";
	$message = "Login First!!";
}
else if(isset($_POST['siteid']) && isset($_POST['magid']) && isset($_POST['from'])){
	$siteid = $_POST['siteid'];
	$magid = $_POST['magid'];
	$from = $_POST['from'];
	$userid = $_SESSION['user_id'];
	include 'connect.php';
	$query = "UPDATE mashup_sites_$userid SET sites_magazine_id = ? WHERE sites_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($magid,$siteid));
	$query = "UPDATE mashup_magazines_$userid SET magazines_site_count = magazines_site_count+1 WHERE magazines_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($magid));
	$query = "UPDATE mashup_magazines_$userid SET magazines_site_count = magazines_site_count-1 WHERE magazines_id = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($from));
	$status = "Success";
	$message = "Site Successfully moved";
}
else{
	$status = "error";
	$message = "Improper Parameters passed";
}
$response['status'] = $status;
$response['message'] = $message;
echo json_encode($response);
?>