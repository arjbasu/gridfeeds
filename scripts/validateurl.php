<?php
session_start();
	if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
		$status = "unauthorized";
		$message = "Login First!!";
	}
	else if(isset($_GET['siteurl'])){
		include 'connect.php';
		include '../phpquery/phpQuery.php';
		$user_id = $_SESSION['user_id'];
		$url = $_GET['siteurl'];
		require_once('../php/autoloader.php');
		if(strpos($url,"http://") === false)
			$url = "http://".$url;
		if (preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $url)) {
			$feed = new SimplePie();
			$feed->set_feed_url($url);
			if(!$feed->error()){
				$status = "Success";
				$message = "Feed Exists";
			}
			else{
				$ch = curl_init();
				$timeout = 20;
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
				$data = curl_exec($ch);
				curl_close($ch);
				if(!$data){
					$status = "error";
					$message = "Unable to open URL";
				}
				else{
					$doc = phpQuery::newDocumentHTML($data);
					phpQuery::selectDocument($doc);
					$flag = 0;
					foreach(pq('link') as $link) {
						$relattribute =  $link->getAttribute("rel");
						if($relattribute == "alternate"){
							$flag = 1;
							break;
						}
							
					}
					if($flag ==0){
						$status = "error";
						$message = "Site does not provide RSS feeds";
					}
					else{
						$status = "Success";
						$message = "Site provides RSS feeds";
					}
				}		
			}
			
		}
		else
		{
			$status = "error";
			$message = "Invalid URL";
			
		}
		
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
		echo $message;
	}
	
	$response['status'] = $status;
	$response['message'] = $message;
	echo json_encode($response);
?>