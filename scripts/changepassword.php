<?php
session_start();
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	$status = "unauthenticated";
	$message = "Login First!!";
}

else if(isset($_POST['currentPass']) && isset($_POST['newPass'])){
	include 'connect.php';
	$userid = $_SESSION['user_id'];
	$currentPassword = $_POST['currentPass'];
	$newPassword = $_POST['newPass'];
	$confirmPassword = $_POST['confirmPass'];
	$hashedpass = crypt($currentPassword,'$1$foreverdope12$');
	$query = "SELECT * FROM mashup_users WHERE user_id = ? AND user_password = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($userid,$hashedpass));
	if($stmt->rowCount() == 1){
		if($confirmPassword != $newPassword){
			$status = "Error";
			$message = "Passwords do not match";
		}
		else{
			$hashednew = crypt($newPassword,'$1$foreverdope12$');
			$query = "UPDATE mashup_users SET user_password = ? WHERE user_id = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($hashednew,$userid));
			$status = "Success";
			$message = "You have successfully changed your password";
		}
	}
	else{
		$status = "Error";
		$message = "Incorrect password supplied";
	}
}

else{
	$status = "Error";
	$message = "Improper parameters passed";
}

$response['status'] = $status;
$response['message'] = $message;
echo json_encode($response);
?>