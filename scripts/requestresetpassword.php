<?php
	if(isset($_POST['email'])){
		include 'connect.php';
		$email = $_POST['email'];
		$query = "SELECT user_id FROM mashup_users WHERE user_email = ?";
		$message['query'] = $email;
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute(array($email));
		if($stmt->rowCount() >= 1){
			$user = $stmt->fetch();
			$user_id = $user['user_id'];
			$hash = md5($user_id.$email.date('m/d/Y h:i:s a', time()));
			$querynew = "UPDATE mashup_users SET user_password_reset_status=1, user_password_reset_id='$hash' ".
			"WHERE user_id = '$user_id'";
			$result = mysql_query($querynew);
			if(!$result){
				$status = "Error";
				$response = "Error generating password reset link";
			}
			else{
				$subject = "[GridFeeds] Password Reset Link";
				$content = "Your password reset link is:\n\n".
				"http://www.foreverdope.com/mashup/resetpassword.php?email=$email&hash=$hash\n\n".
				"Please visit the above link to reset your password. If you did not request for a password\n".
				"reset, then ignore this email";
				$from = "no-reply@gridfeeds.com";
				$headers = "From:" . $from;
				mail($email, $subject, $content,$headers);
				$status = "Success";
				$response = "Password reset link has been successfully sent to $email. Please follow the".
				" instructions provided in the mail";
			}
			
		}
		else{
			$status = "Error";
			$response = "No records found";
		}
		
	}
	else{
		$status = "Error";
		$response = "Improper parameters passed";
	}
	$message['status'] = $status;
	$message['response'] = $response;
	echo json_encode($message);
?>