<?php
	session_start();
	include 'connect.php';
	if(isset($_POST['email']) && isset($_POST['password'])){
		$remember = $_POST['rememberme'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$hashedpassword = crypt($password,'$1$foreverdope12$');
		$query = "SELECT user_name,user_verified,user_id FROM mashup_users WHERE user_email = ? AND user_password = ?";
		$stmt = $pdo->prepare($query);
		$result = $stmt->execute(array($email,$hashedpassword));
		if($stmt->rowCount() == 1){
			$result = $stmt->fetch();
			$status = $result['user_verified'];
			$name = $result['user_name'];
			$user_id = $result['user_id'];
			
			if($status == 0){
				$status = "Failure";
				$response = "You have not yet verified your email. Please click on the link sent to your mail
				to verify your email";
			}
			else{
				if($remember == "remember"){
					setcookie("user",$name,time()+14*24*60*60,"/");
					setcookie("user_id",$user_id,time()+14*24*60*60,"/");
				}
					
				$_SESSION['logged'] = true;
				$_SESSION['username'] = $name;
				$_SESSION['user_id'] = $user_id;
				$status = "Success";
				$response = "You have successfully logged in. One moment...";
			}
		}
		else{
			$status = "Failure";
			$response = "Incorrect email/password combination";
		}
	}
	else{
		$status = "Failure";
		$response = "Improper parameters passed";
	}
	$message['status'] = $status;
	$message['response'] = $response;
	$message['remember'] = $remember;
	$message['query'] = $query;
	echo json_encode($message);
?>