<?php
	session_start();
	if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
		$status = "unauthorized";
		$message = "Login First!!";
	}
	else if(isset($_POST['magazinename'])){
		$magid = "";
		include 'connect.php';
		$user_id = $_SESSION['user_id'];
		$magname = $_POST['magazinename'];
		if($magname == ""){
			$status = "error";
			$message = "magazine name empty";
		}
		else{
			$tablename = "mashup_magazines_$user_id";
			$query = "SELECT * FROM $tablename WHERE magazines_name = ?";
			$stmt = $pdo->prepare($query);
			$stmt->execute(array($magname));
			if($stmt->rowCount() == 1){
				$status = "error";
				$message = "magazine exists";
			}
			else{
				$query = "INSERT INTO $tablename(magazines_name) VALUES(?)";
				$stmt = $pdo->prepare($query);
				$stmt->execute(array($magname));
				if($stmt->rowCount() == 1){
					$query = "SELECT * FROM $tablename ORDER BY magazines_timeofadding DESC LIMIT 1";
					$result = mysql_query($query);
					if(!$result){
						$status = "error";
						$message = "Unable to query database";
					}
					else{
						$temp = mysql_fetch_assoc($result);
						$magid = $temp['magazines_id'];
						$magname = $temp['magazines_name'];
						$status = "Success";
						$message = "Magazine Successfully added";
					}
				}
				else{
					$status = "error";
					$message = "Error adding magazine";
				}	
			}
		}
	}
	else{
		$status = "error";
		$message = "Improper parameters passed";
	}
	$response['status'] = $status;
	$response['message'] = $message;
	$response['magazine_id'] = $magid;
	$response['magazine_name'] = $magname;
	echo json_encode($response);
?>