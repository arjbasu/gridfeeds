<?php
session_start();
if(!(isset($_SESSION['logged']) && isset($_SESSION['username']) && isset($_SESSION['user_id']))){
	$status = "Error";
	$message = "Unauthenticated";
}
else if(isset($_POST['title']) && isset($_POST['description']) && isset($_POST['date'])
		&& isset($_POST['source']) && isset($_POST['thumbnail']) && isset($_POST['link'])){
	$userid = $_SESSION['user_id'];
	$title = $_POST['title'];
	$description = $_POST['description'];
	$date = $_POST['date'];
	$source = $_POST['source'];
	$thumbnail = $_POST['thumbnail'];
	$link = $_POST['link'];
	include 'connect.php';
	$query = "SELECT * FROM mashup_favorites_$userid WHERE favorites_date = ?";
	$stmt = $pdo->prepare($query);
	$stmt->execute(array($date));
	if($stmt->rowCount() >=1 ){
		$status = "Success";
		$message = "Exists";
	}
	else{
		$query = "INSERT INTO mashup_favorites_$userid(favorites_title,favorites_description,".
				"favorites_date,favorites_source,favorites_link,favorites_image) VALUES(?,?,?,?,?,?)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($title,$description,$date,$source,$link,$thumbnail));
		$status = "Success";
		$message = "Favorite Succesfully added";
	}
}
else{
	$status = "Error";
	$message = "Improper Parameters passed";
}
$response['status'] = $status;
$response['message'] = $message;
echo json_encode($response);
?>