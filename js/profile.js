function adjust(){
	img = $("#dp")[0];
	var nath,natw;
	if (typeof img.naturalWidth == "undefined") {  
		var tempimg = new Image();  
	    tempimg.src = img.src;  
	    natw = img.width;  
	    nath = img.height;
		
	}
	else{
		nath = img.naturalHeight;
		natw = img.naturalWidth;
	}
	width = (180/nath)*natw;
	boxwidth = $("#dpspot").width();
	dif = boxwidth-width-20;
	dif = dif+"px";
	$("#profiledetails").css({"right":dif,"visibility":"visible"});
	img.width = width;
	img.height = 180;
	$("#dp").css("visibility","visible");
}


$("document").ready(function(){
	$(".contents").mCustomScrollbar();
    $(".contents .mCSB_scrollTools").hide();
    $(".contents").hover(function(){
    	$(this).find(".mCSB_scrollTools").show();
    	},
    	function(){
    		$(this).find(".mCSB_scrollTools").hide();
    });
    
    function init(){
    	$("body").ajaxStart(function(){
    		$("#magazineSelect").find(".success,.error").hide();
    		$("#successdiv,#errordiv").hide();
    	});
    	$("#magazineSelect").on("hidden",function(){
    		$("#magazinelistForm").show();
    		$("#magazineSelect").find(".success,.error").hide();
    	})
    	$("body").delegate(".addsite","click",function(){
        	id = this.id.substr(4);
        	console.log(id);
        	$("#magazineSelect").modal('show');
        	magid = $(this).parents(".magazinedetails")[0].id.substr(8);
        	console.log(magid);
        	url = magazinelist[magid].sites[id].sites_feed_url;
        	$("#formsiteurl").val(url);
        })
        .delegate(".formmagazinename","click",function(){
        	magid = this.id.substr(3);
        	siteurl = $("#formsiteurl").val();
        	$("#magazineSelect").mask("Adding Site...",200);
        	data = "magazine_id="+magid+"&siteurl%5B%5D="+encodeURIComponent(url);
        	$.ajax({
        		url:"scripts/addsite.php",
    			data:data,
    			type:"POST",
    			dataType:"JSON",
    			success:function(data){
    				if(data.status == "unauthenticated"){
    					window.location = "index.php";
    				}
    				else if(data.status == "Success"){
    					$("#magazineSelect").unmask();
    					$("#magazinelistForm").slideUp("100");
    					$("#magazineSelect .success p").html("Site Successfully added").parent().slideDown("200");
    				}
    				else{
    					$("#magazineSelect .error p").html(data.message).parent().slideDown("200");
    					$("#magazineSelect").unmask();
    				}
    			},
    			error:function(){
    				$("#magazineSelect .error p").html("Error Adding Site").parent().slideDown("200");
    				$("#magazineSelect").unmask();
    			}
        	})
        })
        .delegate(".friendAdd","click",function(){
        	id = this.id.substr(4);
        	data = {"friend_id":id,"action":"add"};
        	$(this).mask("Adding...",200)
        	$.ajax({
        		url:"scripts/friendaction.php",
        		data:data,
        		type:"POST",
        		dataType:"JSON",
        		success:function(data){
        			if(data.status == "unauthorized"){
        				window.location = "index.php";
        			}
        			else if(data.status == "success"){
        				$(".friendAdd").removeClass("btn-primary").addClass("btn-success").addClass("friendRemove").html("Following").unmask();
        				$(".friendRemove").removeClass("friendAdd");
        			}
        			else{
        				$(".friendAdd").unmask();
        				$("#errordiv p").html("unable to add ").parent().slideDown("200");
        			}
        		},
        		error:function(){
        			$(".friendAdd").unmask();
        			$("#errordiv p").html("unable to add ").parent().slideDown("200");
        		}
        	});
        })
        .delegate(".friendRemove","mouseenter",function(){
        	$(this).removeClass("btn-success").addClass("btn-danger").html("Unfollow");
        })
        .delegate(".friendRemove","mouseleave",function(){
        	$(this).removeClass("btn-danger").addClass("btn-success").html("Following");
        })
        .delegate(".friendRemove","click",function(){
        	id = this.id.substr(4);
        	data = {"friend_id":id,"action":"delete"};
        	$(this).mask("Removing...",200)
        	$.ajax({
        		url:"scripts/friendaction.php",
        		data:data,
        		type:"POST",
        		dataType:"JSON",
        		success:function(data){
        			if(data.status == "unauthorized"){
        				window.location = "index.php";
        			}
        			else if(data.status == "success"){
        				$(".friendRemove").removeClass("btn-success").removeClass("btn-danger").addClass("friendAdd").addClass("btn-primary").html("Follow");
        				$(".friendRemove").unmask();
        				$(".friendAdd").removeClass("friendRemove");
        			}
        			else{
        				$(".friendRemove").unmask();
        				$("#errordiv p").html("unable to Unfollow").parent().slideDown("200");
        			}
        		},
        		error:function(){
        			$(".friendAdd").unmask();
        			$("#errordiv p").html("unable to Unfollow").parent().slideDown("200");
        		}
        	});
        });
        $(".site").hover(function(){
        	id = this.id
        	html = "<button class = 'addsite btn btn-mini' id = 'site" + id + "'><i class = 'icon-plus'></i></button>"
        	$(this).append(html);
        },function(){
        	$(this).find("button").remove();
        });
    }
    if(self!=true){
    	init();
    }
});