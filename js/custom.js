$(document).ready(function(){
	$("#loginButton").click(function(){
		$(".login").show();
		$(".signup").hide();
		$("#loginmodal").modal('show');
		return false;
	});
	
	$("#loginmodal").on("hidden",function(){
		$(".resetPassword,#resetPassworddiv,#loading,#successdiv,#resetPasswordstatus").hide();
		$("#loginForm,#signupForm,#resetPasswordForm").trigger("reset");
		if($("#resetPassworddiv").hasClass("alert-success")){
			$("#resetPassworddiv").removeClass("alert-success")
		}
		if($("#resetPassworddiv").hasClass("alert-danger")){
			$("#resetPassworddiv").removeClass("alert-danger")
		}
	});
	
	
	$("#forgotPassword").click(function(){
		$(".login").hide();
		$(".resetPassword").fadeIn("200");
		return false;
	});
	
	$("#resetPasswordButton").click(function(){
		$("#resetPasswordForm").trigger('submit');
		$(".resetPassword").not(":eq(0)").hide();
		$("#loading").fadeIn("300");
		return false;
	});
	
	$("#loginProceed").click(function(){
		$("#resetPassworddiv").hide().removeClass("alert-danger");
		$("#loginForm").trigger("submit");
		return false;
	});
	$("#loginForm").submit(function(){
		$("#loading").fadeIn("300");
		data = $(this).serialize();
		$.ajax({
			url:"scripts/login.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "Success"){
					$("#resetPassworddiv").addClass("alert-success");
					$("#resetPasswordStatus").html(data.response);
					setTimeout(function(){
					 window.location = "234.php";
					},800);
				}
				else{
					$("#resetPassworddiv").addClass("alert-danger");
					$("#resetPasswordStatus").html(data.response);
				}
				$("#loading").hide();
				$("#resetPassworddiv").show();
			},
			error:function(){
				$("#resetPassworddiv").addClass("alert-danger");
				$("#resetPasswordStatus").html("Error in connection");
				$("#loading").hide();
				$("#resetPassworddiv").show();
			}
		})
		return false;
	});
	
	$("#resetPasswordForm").submit(function(){
		data = $(this).serialize();
		$.ajax({
			url:"scripts/requestresetpassword.php",
			type:"POST",
			data:data,
			dataType:"JSON",
			success:function(msg){
				if(msg.status === "Success"){
					$("#resetPassworddiv").addClass("alert-success");
					$("#resetPasswordStatus").html(msg.response);
				}
				else{
					$("#resetPassworddiv").addClass("alert-danger");
					$("#resetPasswordStatus").html(msg.response);
				}
				$("#loading").hide();
				$("#resetPassworddiv").fadeIn("300");
			},
			error:function(){
				$("#resetPassworddiv").addClass("alert-danger");
				$("#resetPasswordStatus").html("Unable to connect");
				$("#loading").hide();
				$("#resetPassworddiv").fadeIn("300");
			}
		});
		return false;
	});
	
	$("#signupButton").click(function(){
		$(".login").hide();
		$(".signup").show();
		$("#loginmodal").modal("show");
	});
	$("#signupProceed").click(function(){
		$("#signupForm").trigger("submit");
	});
	$("#signupForm").submit(function(){
		$(".signup").slideUp("300");
		$("#loading").fadeIn("200");
		data = $(this).serialize();
		$.ajax({
			url:"scripts/signup.php",
			type:"POST",
			dataType:"json",
			data:data,
			success:function(data){
				if(data.status == "Success"){
					$("#maillink").html(data.response);
					$("#loading").fadeOut("300");
					$("#successdiv").fadeIn("500");
				}
			}
		});
		return false;
	});
	
	$("#showLogin").click(function(){
		$(".signup").fadeOut("700",function(){
			$(".login").fadeIn("500");
		});
		return false;
	});
	$("#loginmodal").on('hidden',function(){
		$(".login").hide();
		$(".signup").show();
	});
	
	if($(".magazine").length>0){
		$(".magazine:eq(0)").parent().find(".subMagazine").slideDown("700");
	}
	$(".magazine").click(function(){
		if($(this).parent().find(".subMagazine:visible").length<=0){
			$(".subMagazine:visible").slideUp("150",$.proxy(function(){
				$(".magazine").parent().removeClass("active");
				$(this).parent().addClass("active").find(".subMagazine").slideDown("400");
			},this));
		}
		return false;
	});
});
