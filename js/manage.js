(function($){
    	$(window).load(function(){
    		
    		$("body").ajaxStart(function(){
    			$("#successdiv,#errordiv,.success,.error").slideUp("150").find("p")
    			.removeClass("pull-left,pull-right,pull-center").addClass("pull-center");
    		})
    		$("body").keypress(function(event){
				if(event.which == 13){
					if($("#rename:visible").length>0){
						$("#rename").trigger("click");
					}
				}
			});
    		
    		$("#magazineSelect").on("hidden",function(){
    			$(this).find(".success,.error").hide();
    		})
    		
    		$("#magazineeditmodal").on("hidden",function(){
    			$(this).find(".success,.error").hide();
    		});
    		
    		$("#magazineForm").submit(function(){
    			$("#successdiv,#errordiv").slideUp("100");
				$("#magloading").fadeIn("200",function(){
					data = $("#magazineForm").serialize();
					name = $("#magazineForm #magazineName").val();
					if(name === "" || null === name){
						$("#errordiv p").removeClass("pull-right").addClass("pull-left")
						.html("Magazine Name cannot be empty").parent().slideDown("200");
						$("#magloading").hide();
					}
					else{
						console.log(data);
						$.ajax({
							url:"scripts/addmagazine.php",
							data:data,
							type:"POST",
							dataType:"JSON",
							success:function(data){
								if(data.status == "unauthorized"){
	    							window.location = "index.php";
	    						}
								if(data.status == "Success"){
									$("#magadd").trigger("click");
									$("#successdiv p").html("Magazine Successfully added").parent()
									.slideDown("200");
									window.location = "";
									
								}
								else{
									$("#errordiv p").html(data.message).parent().slideDown("300",function(){
										$("#magadd").trigger("click");
	    								$("#magazineForm").trigger('reset');
	    								$("#magloading").hide();
									});
								}
							},
							error:function(){
								$("#errordiv p").append("Error Occurred").parent().slideDown("300",function(){
									$("#magadd").trigger("click");
									$("#magazineForm").trigger('reset').show();
									$("#magloading").hide();
								});
							}
						});
					}
    			});
    			return false;
    		});
    		
    		$("body").delegate(".draggable","mouseenter",function(){   			
    			$(this).append("<button class = 'btn btn-danger btn-mini deletesite' style = 'margin:5px;'><i class = 'icon-trash'></i></button>")
    		}).
    		delegate(".draggable","mouseleave",function(){    			
    			$(this).find("button").remove();
    		})
    		.delegate(".magtitle","mouseenter",function(){
    			html = "<button class = 'btn editmag' style = 'margin:5px;'><i class = 'icon-edit'></i></button>" +
    			"<button class = 'btn btn-danger deletemag' style = 'margin:5px;'><i class = 'icon-trash'></i></button>";
    				
    			$(this).append(html);
    		})
    		.delegate(".magtitle","mouseleave",function(){
    			$(this).find("button").remove();
    		})
    		.delegate(".editmag","click",function(){
    			element = $(this).parents(".magazinecell").find(".scrollable");
    			id = $(element)[0].id.substr(3);
    			$("#mageditid").val(id);
    			magname = $(this).parent().find(".mainmagname").html();
    			$("#mageditname").val(magname);
    			$("#magazineeditmodal").modal("show");
    		})
    		.delegate("#rename","click",function(){
    			$("#magazineeditmodal").mask("Renaming...",200);
    			data = $("#mageditform").serialize();
    			$.ajax({
    				url:"scripts/renamemagazine.php",
    				type:"POST",
    				data:data,
    				dataType:"JSON",
    				success:function(data){
    					if(data.status == "unauthorized"){
    						window.location = "index.php";
    					}
    					else if(data.status == "success"){
    						$("#magazineeditmodal .success p").html("Magazine successfully renamed").parent().slideDown();
    						window.location = "";
    					}
    					else{
    						$("#magazineeditmodal").unmask();
    						$("#magazineeditmodal .error p").html(data.message).parent().slideDown();
    					}
    				},
    				error:function(){
    					$("#magazineeditmodal").unmask();
						$("#magazineeditmodal .error p").html("Error Occurred").parent().slideDown();
					}
    			})
    		})
    		.delegate(".deletemag","click",function(){
    			$(this).html("<img src = 'images/ajax-loader.gif'></img'");
    			element = $(this).parents(".magazinecell").find(".scrollable");
    			console.log(element);
    			id = $(element)[0].id.substr(3);
				$.ajax({
					url:"scripts/remove.php",
    				type:"POST",
    				data:{"id":id,"type":"magazines"},
    				dataType:"JSON",
    				success:function(data){
    					if(data.status == "unauthenticated"){
    						window.location = "index.php";
    					}
    					if(data.status == "Success"){
    						$("#successdiv p").html("Magazine successfully deleted....").parent().slideDown();
    						window.location = "";
    					}
    					else{
    						$("#errordiv p").html(data.message).parent().slideDown("300");
    					}
    				},
    				error:function(){
    					$("#errordiv p").html("Error Occurred").parent().slideDown("300");
    				}
				});
    		})
    		.delegate(".deletesite","click",function(){
    			current = this;
    			element = $(this).parent();
    			id = element[0].id.substr(4);
    			$(this).html("<img src = 'images/small-loader-delete.gif'></img>");
    			$.ajax({
    				url:"scripts/remove.php",
    				type:"POST",
    				data:{"id":id,"type":"sites"},
    				dataType:"JSON",
    				success:function(data){
    					if(data.status == "unauthenticated"){
    						window.location = "index.php";
    					}
    					if(data.status == "Success"){
    						$("#successdiv p").html("Site successfully deleted..").parent().slideDown("200");
    						$(element).fadeOut("200").remove();
    					}
    					else{
    						$("#errordiv p").html(data.message).parent().slideDown("300");
    					}
    				},
    				error:function(){
    					$("#errordiv p").html("Error Occurred").parent().slideDown("300");
    				}
    			})
    		})
    		
    		$(".formmagazinename").click(function(){
    			id = this.id.substr(3);
    			url = $("#formsiteurl").val();
    			$("#magazineSelect").mask("Adding Site.....");
    			data = "magazine_id="+id+"&siteurl%5B%5D="+encodeURIComponent(url);
    			$.ajax({
					url:"scripts/addsite.php",
					type:"POST",
					data:data,
					dataType:"JSON",
					success:function(data){
						if(data.status == "unauthorized"){
							window.location = "index.php";
						}
						if(data.status == "Success"){
							$("#magazineSelect").unmask();
							$("#magazineSelect .success p").html("Site Added Succesfully...").parent().slideDown("200");
							window.location = "";
						}
						else{
							$("#magazineSelect").unmask();
							$("#magazineSelect .error p").html(data.message).parent().slideDown("200");
						}
					},
					error:function(){
						$("#magazineSelect").unmask();
						$("#magazineSelect .error p").html("Error Occurred").parent().slideDown("200");
					}
				});
    			
    		});
    		$("#siteForm").submit(function(){
    			$("#successdiv,#errordiv").slideUp("100");
    				url = $("#siteUrl").val();
    				magid = $("#magselect").val();
    				if(url == "" || magid == "NULL"){
    					$("#errordiv p").removeClass("pull-right").addClass("pull-left")
						.html("Site url cannot be empty")
						.parent().slideDown("200");
    					return false;
    				}
    				else{
    					$("#siteloading").fadeIn("200",function(){
    						$("#formsiteurl").val(url);
    						$("#siteloading").fadeOut("200",function(){
								$("#siteadd").trigger('click');
								$("#siteForm").trigger('reset');
								$("#magazineSelect").modal("show");
    						});
    	    			});
        				
    				}
    				
    			return false;
    		});
    		
            var currentMousePos = { x: -1, y: -1 };
            $(document).mousemove(function(event) {
                currentMousePos.x = event.pageX;
                currentMousePos.y = event.pageY;
            });
            
        	$(".draggable").draggable({helper:"clone",
        		start:function(event,ui){
        			$("#errordiv").slideUp("200");
        			ui.helper.css("display","none");
        			x = currentMousePos.x-15;
        			y = currentMousePos.y-15;
        			$(ui.helper).parent().find("button").remove();
        			$("#dragSelection").html(ui.helper.html()).css({"top":y,"left":x}).show();
        			//ui.helper.css("cursor","move");
        		},
        		drag:function(event,ui){
        			x = currentMousePos.x-15;
        			y = currentMousePos.y-15;
        			$("#dragSelection").css({"top":y,"left":x})
        		},
        		stop:function(event,ui){
        			$("#dragSelection").hide();
        		}
        	});
            $(".droppable").droppable({
                drop: function( event, ui ) {
                		if($(ui.draggable).parents(".scrollable").attr("id") === this.id){
                			return false;
                		}
                		else{
                			$("body").mask("Moving...",200);
                			from = $(ui.draggable).parents(".scrollable").attr("id");
                			siteid = ui.draggable[0].id;
                			id = siteid.substr(4);
                			magid = this.id.substr(3);
                			from = from.substr(3);
                			current = this;
                			data = {"siteid":id,"magid":magid,"from":from};
                			$.ajax({
                				url:"scripts/movesite.php",
                				type:"POST",
                				data:data,
                				dataType:"JSON",
                				success:function(data){
                					if(data.status == "unauthorized"){
            							window.location = "index.php";
            						}
                					if(data.status == "Success"){
                						$(current).find(".text").append("<p id = '"+ siteid +"'></p>").find("p:last").addClass("draggable").draggable({helper:"clone",
            	                			start:function(event,ui){
            	                				$("#errordiv").slideUp("200");
            	                    			ui.helper.css("display","none");
            	                    			x = currentMousePos.x-15;
            	                    			y = currentMousePos.y-15;
            	                    			$(ui.helper).parent().find("button").remove();
            	                    			$("#dragSelection").html(ui.helper.html()).css({"top":y,"left":x}).show();
            	                    			//ui.helper.css("cursor","move");
            	                    		},
            	                    		drag:function(event,ui){
            	                    			x = currentMousePos.x-15;
            	                    			y = currentMousePos.y-15;
            	                    			$("#dragSelection").css({"top":y,"left":x})
            	                    		},
            	                    		stop:function(event,ui){
            	                    			$("#dragSelection").hide();
            	                    		}
            	                		}).append(ui.helper.html());
            	                		$(ui.draggable).parents(".scrollable").mCustomScrollbar("update");
            	                		$(ui.draggable).remove();
            	                		$(current).mCustomScrollbar("update");
            	                		$("body").unmask();
                					}
                					else{
                						$("#errordiv p").html(data.message).parent().slideDown("300");
                						return false;
                					}
                				},
                				error:function(){
                					$("#errordiv p").html("Error moving site").parent().slideDown("300");
                				}
                				
                			});
                			
                		}
                }
            });

            $(".scrollable").mCustomScrollbar();
            $(".scrollable .mCSB_scrollTools").hide();
            $(".scrollable").hover(function(){
            	$(this).find(".mCSB_scrollTools").show();
            	},
            	function(){
            		$(this).find(".mCSB_scrollTools").hide();
            });
            $(".mCustomScrollBox .mCSB_scrollTools .mCSB_dragger").hover(function(){
            	$(".mCustomScrollBox .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar").css("background-color","#2F96B4");
            },function(){
            	$(".mCustomScrollBox .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar").css("background-color","#4AB0CE");
            });
     });
})(jQuery);
