$(document).ready(function(){
	var index = 0;
	var flag = 1;
	$("#magazineForm").submit(function(){
		data = $(this).serialize();
			$("#magazineForm,#magazineHeader,#errordiv,#successdiv").slideUp("300");
			
			$("#loading").fadeIn("200");
			$.ajax({
				url:"scripts/addmagazine.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "Success"){
						$("#loading").fadeOut("300",function(){
							$("#magazineid").val(data.magazine_id);
							$("#successdiv").html("<p class = 'pull-center'>"+data.message+"</p>").fadeIn("400");
							$("#magazineForm,#magazineHeader").remove();
							$(".bar").css("width","33%");
							$("#websiteForm,#websiteHeader").slideDown("300");
						});
					}
					else{
						$("#errordiv").html("<p class = 'pull-center'>"+data.message+"</p>").fadeIn("400");
						$("#loading").fadeOut("300",function(){
							$("#magazineForm,#magazineHeader").slideDown("300");
						});
					}
				},
				error:function(){
					message = "Unable to connect";
					$("#errordiv").html("<p class = 'pull-center'>"+message+"</p>").fadeIn("400");
					$("#loading").fadeOut("300",function(){
						$("#magazineForm,#magazineHeader").slideDown("300");
					});
				}
			});
			return false;
		});
	$(".tooltiptrigger").click(function(){
		return false;
	});
		
		function ajaxvalidate(index,url,formdata){
			console.log("In ajaxvalidate with index:"+index);
			if(index<4){
				$(".loadingimg").eq(index).fadeIn("200");
				if(url[index]!= ""){
					$.ajax({
						url:"scripts/validateurl.php",
						type:"GET",
						data:{"siteurl":url[index]},
						dataType:"JSON",
						async:"false",
						success:function(data){
							if(data.status == "Success"){
								console.log("success");
								$(".loadingimg").eq(index).hide();
								$(".successimg").eq(index).show();
								ajaxvalidate(index+1,url,formdata);
								//$(".tooltiptrigger").eq(index).attr("title",data.message);
							}
							else{
								
								$(".loadingimg").eq(index).hide();
								$(".failureimg").eq(index).show();
								//$(".tooltiptrigger").eq(index).attr("title",data.message)//.tooltip('show');
								ajaxvalidate(index+1,url,formdata);
								flag = 0;
							}
						},
						error:function(){
							
							$(".loadingimg").eq(index).hide();
							$(".failureimg").eq(index).show();
							ajaxvalidate(index+1,url,formdata);
							//$(".tooltiptrigger").eq(index).attr("title",data.message).tooltip("show");
							flag = 0;
						}
					});
				}
				else{
					ajaxvalidate(index+1,url,formdata);
				}
			}
			else{
				ajaxSubmit(formdata);
			}
		}
		
		function ajaxSubmit(formdata){
			if(flag == 1){
				$("#websiteForm,#websiteHeader").slideUp("300");
				$("#loading").fadeIn("200");
				console.log(formdata);
				$.ajax({
					url:"scripts/addsite.php",
					type:"POST",
					data:formdata,
					dataType:"JSON",
					success:function(data){
						if(data.status == "Success"){
							$("#loading").fadeOut("300",function(){
								$("#successdiv").html("<p class = 'pull-center'>"+data.message+"</p>").fadeIn("400");
								$("#websiteForm,#websiteHeader").remove();
								$(".bar").css("width","66%");
								$("#personalForm,#personalHeader").slideDown("300");
							});
						}
						else{
							$("#errordiv").html("<p class = 'pull-center'>"+data.message+"</p>").fadeIn("400");
							$("#loading").fadeOut("300",function(){
								$("#websiteForm,#websiteHeader").slideDown("300");
							});
						}
					},
					error:function(){
						message = "Unable to connect";
						$("#errordiv").html("<p class = 'pull-center'>"+message+"</p>").fadeIn("400");
						$("#loading").fadeOut("300",function(){
							$("#websiteForm,#websiteHeader").slideDown("300");
						});
					}
				});
			}
		}
		$("#websiteForm").submit(function(){
			flag = 1;
			$("#successdiv,#errordiv").slideUp("300");
			$(".loadingimg,.successimg,.failureimg").hide();
			formdata = $(this).serialize();
			url = new Array();
			$(".siteurl").each(function(){
				tempurl = $(this).val();
				url.push(tempurl);
			});
			ajaxvalidate(index,url,formdata)
			return false;
		});
		
		$("#personalForm").submit(function(){
			$("#personalForm,#personalHeader").slideUp("300",function(){
				$("#loading").show();
			});
			
			file = $("#profiledp").val();
			if(file != ""){
				var fileext = file.split(".");
				extension = fileext[fileext.length -1 ];
				if(extension!== 'jpg' && extension!== 'jpeg' && extension!== 'gif' && extension!== 'png'){
					html = "<p class = 'pull-center'>You can only upload an image in jpg,jpeg,gif or png formats</p>";
					$("#errordiv").html(html).show();
					return false;
				}
				var imageupload;
				$.ajaxFileUpload
				(
					{
						url:'scripts/uploadimage.php',
						fileElementId:'profiledp',
						dataType: 'json',
						success: function (data,status,e)
						{
							if(data.status == "Error"){
								html = "<p class = 'pull-center'>There was an error uploading the image.Please try again</p>";
								$("#errordiv").html(html).show();
								$("#personalForm,#personalHeader").slideDown("300");
								
							}
							else{
								var filename = data.message;
								console.log(filename);
								bio = $("#bio").val();
								$.ajax({
									url:"scripts/updatebio.php",
									type:"POST",
									data:{"image":filename,"bio":bio},
									dataType:"JSON",
									success:function(data){
										if(data.status == "Success"){
											$("#loading").fadeOut("200");
											$("#successdiv").html("<p>"+data.message+"</p>").show();
											$("#personalForm,#personalHeader").remove();
											$(".bar").css("width","100%");
											$("#completeHeader,.completeText,#headInside").fadeIn("300");
										}
										else{
											$("#errordiv").html("<p class = 'pull-center'>"+data.message+"</p>").show();
											$("#personalForm,#personalHeader").slideDown("300");
										}
									},
									error:function(){
										$("#errordiv").html("<p class = 'pull-center'>Internal Error</p>").show();
										$("#personalForm,#personalHeader").slideDown("300");
									}
								});
							}
							
						},
						error: function (data, status, e)
						{
							imageupload.status = "Error";
							$("#errordiv").html("<p class = 'pull-center'>"+data.message+"</p>").show();
							$("#personalForm,#personalHeader").slideDown("300");
						}
					}
				)
			}
			else{
				var bio = $("#bio").val();
				$.ajax({
					url:"scripts/updatebio.php",
					type:"POST",
					data:{"image":"NULL","bio":bio},
					dataType:"JSON",
					success:function(data){
						if(data.status == "Success"){
							$("#loading").fadeOut("200");
							$("#successdiv").html("<p>"+data.message+"</p>").show();
							$("#personalForm,#personalHeader").remove();
							$(".bar").css("width","100%");
							$("#completeHeader,.completeText,#headInside").fadeIn("300");
						}
						else{
							$("#errordiv").html("<p class = 'pull-center'>"+data.message+"</p>").show();
							$("#personalForm,#personalHeader").slideDown("300");
						}
					},
					error:function(){
						$("#errordiv").html("<p class = 'pull-center'>Internal Error</p>").show();
						$("#personalForm,#personalHeader").slideDown("300");
					}
				});
			}
			return false;
		});
});