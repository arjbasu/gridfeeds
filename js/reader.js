$(document).ready(function(){
	$("#lightbox-content #loadbox").width(window.innerWidth-50).height(window.innerHeight);
	$("#lightbox-content #loadbox").load(function(){
		$(this).find("a").each(function(){
			$(this).attr("target","_self");
		})
	});
	var page = 1;
	var pages
	var firsturl = [];
	var feeds = [];
	var called = false;
	$(".sources").hover(function(){
		console.log("hoverin");
		if(!$(this).hasClass("siteselected")){
			$(this).css("background-color","lightgrey");
		}
	},function(){
		console.log("hoverout");
		if(!$(this).hasClass("siteselected")){
			$(this).css("background-color","white");
		}
	});
	$("body").delegate(".viewPost","click",function(){
			id = this.id;
			$("#loadbox").hide();
			$(".lightbox-elements").show();
			$("#lightbox-title").html(feeds[id].title);
			$("#lightbox-source").html(feeds[id].source);
			$("#lightbox-description").html(feeds[id].description);
			$("#readmore").attr("href",feeds[id].link);
			$("#fancybox-content").mCustomScrollbar("update");
	})
	.delegate("#lightbox-description a,#readmore","click",function(){
		url = $(this).attr("href");
		$(".lightbox-elements").fadeOut("400");
		$("#lightbox-content #loadbox").height(window.innerHeight).
		attr("src",url).fadeIn("200");
		$("#fancybox-wrap").width(window.innerWidth-100)
		.find("#fancybox-content").width(window.innerWidth-130);
		$.fancybox.resize();
		console.log("lightbox link clicked");
		return false;
	});
	function calibrate(){
		if(called !== true){
			console.log("calibrate called");
			$("#feedsrow .storygrid").each(function(){
				totalheight = $(this).height();
				headingheight = $(this).find("h3").height();
				if(headingheight < totalheight*0.15){
					spacer = "<div style = 'margin-bottom:13px'><br/></div>";
					$(this).find(".headers").append(spacer);
				}
			});
			called = true;
		}
	}
	function display(){
		called = false;
		favid = $(".active").find(".magazine")[0].id;
		console.log("idhello:"+favid);
		if(favid === "favorites"){
			star = "full_star";
		}
		else{
			star = "empty_star";
		}
		$("#feedsrow").html("");
		console.log(feeds);
		var starid = "";
		for(i = (page-1)*9; i < page*9&&i < feeds.length; i++){
			if(favid == "favorites"){
				starid = feeds[i].id;
			}
			else{
				starid = i;
			}
			if(i%3==0){
				$("#feedsrow").append("<ul class='thumbnails bordered thumbnail-list'></ul>");
			}
			if(feeds[i].caption === "" || null === feeds[i].caption){
				replaced = "No Description Available";
			}
			else{
				replaced = feeds[i].caption.substring(0,70).replace(/&/g, "&amp;")
				.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace("&lsquo;","'")
				.replace("&rsquo;","'").replace("&amp;","&")+ "..";
			}
			if(feeds[i].thumbnail!=""){
				image = feeds[i].thumbnail;
			}
			else{
				image = "images/300x200.gif";
			}
			
			feeds[i].title = feeds[i].title.replace("&amp;","&")
			.replace("&lt;","<").replace("&rt;",">").replace("&lsquo;","'")
			.replace("&rsquo;","'");
			
			if(feeds[i].title.length > 25){
				displaytitle = feeds[i].title.substring(0,23) + "..";
			}
			else{
				displaytitle = feeds[i].title;
			}
			
			if(feeds[i].source.length>20){
				source = feeds[i].source.substring(0,18)+"..";
			}
			else{
				source = feeds[i].source;
			}
			html = '<div class = "headers" style = "background-color:lightblue;margin-bottom:-12px;">'+
			'<div class = "pull-center"><h3 style = "font-size:16px">'+displaytitle+
			'</h3><p style = "font-style:italic;font-size:14px;">'
			+source+'</p></div></div>';
				
			read = '<a href="#" class = "viewPost" id = "'+i+'">'+
	          '<figure class="thumbnail-figure" style = "margin-bottom:5px;">'+
	            '<img src="'+image+'" height = "200" width = "310" alt="">'+
	            '</img>'+
	            '<figcaption class="thumbnail-title">'+
	                '<p class = "pull-right" style = "font-style:italic;font-size:14px;">'+
	                  '<span>'+replaced+'</span>'+
	                '</p>'+
	           '</figcaption>'+
	          '</figure>'+
	       '</a>'+
	       '<p class = "pull-left" style = "margin-left:5px;position:relative;top:10px;font-size:14px">'
	       +feeds[i].date+'</p>'+
	       '<a href = "#" class = "star" id = "star' + starid + '">'+
		       '<img src = "images/' + star + '.png" height = "32" width = "32" class = "pull-right">'+
		       '</img>'+
	       '</a>';
			$("#feedsrow").find("ul").last().append("<li class = 'storygrid span4' style = 'padding:0px;'>"+html+read+"</li>");
		}
		$("#lightbox-title").html(feeds[0].title);
		$("#lightbox-source").html(feeds[0].source);
		$("#lightbox-description").html(feeds[0].description);
//		$("#lightbox-content").mCustomScrollbar("update");
		$("#readmore").attr("href",feeds[0].link);
		$("#feedsrow ul li").css("border-bottom-color","#928C8C;");
		width = $(".thumbnail-figure").width();
		console.log("width:"+width);
		$(".thumbnail-figure img").height("200px").width("310px").css("opacity","0.0");
		$("#body-loading").fadeOut("200",function(){
			$("#feedsrow,.controls").fadeIn("300",function(){
				width = $(".thumbnail-figure").width();
				console.log("width:"+width);
				$(".thumbnail-figure img").width(width)
				.width("100%")
				.animate({"opacity":"1.0"},100);
				//calibrate();
				if(page == 1){
					$("#previous").hide();
				}
				if(page == pages){
					$("#next").hide();
				}
			});
		});
		$(".viewPost").attr("href","#lightbox-content");
		$(".viewPost").fancybox({
			height:"500",
			width:"560",
			scrolling:"auto",
			autoDimensions:"true",
			centerOnScroll:"true",
			transitionIn:"fade",
			transitionOut:"fade",
			enableEscapeButton:"true",
			onStart:function(){
				$("#fancybox-wrap").width(window.innerWidth-100)
				.find("#fancybox-content").width(window.innerWidth-130);
			},
			onComplete:function(){
				$("body").css("overflow","hidden");
				$.fancybox.resize();
				},
			onClosed:function(){
				$("body").css("overflow","auto");
				}
		});
		$("#fancybox-content").css({"max-height":window.innerHeight-50,"overflow":"auto"});
		$("#fancybox-content").mCustomScrollbar();
	    $("#fancybox-content .mCSB_scrollTools").hide();
	    $("#fancybox-content").hover(function(){
	    	$(this).find(".mCSB_scrollTools").show();
	    	},
	    	function(){
	    		$(this).find(".mCSB_scrollTools").hide();
	    });
	    $(".mCustomScrollBox .mCSB_scrollTools .mCSB_dragger").hover(function(){
	    	$(".mCustomScrollBox .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar").css("background-color","#2F96B4");
	    }),function(){
	    	$(".mCustomScrollBox .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar").css("background-color","#4AB0CE");
	    }
	}
	
	$("#favorites").click(function(){
		$(".magazine").parent().removeClass("active");
		$("#feedsrow,.controls").fadeOut("200",function(){$("#body-loading").fadeIn("200");})
		$(".subMagazine:visible").slideUp("150",function(){
			$("#favorites").parent()
			.addClass("active").find(".subMagazine").slideDown("200");
			$.ajax({
				url:"scripts/getfavorites.php",
				type:"GET",
				dataType:"JSON",
				success:function(data){
					if(data.status == "unauthenticated"){
						window.location = "index.php";
					}
					if(data.status == "Zeroresults"){
						$("#body-loading").fadeOut("200",function(){
							$("#feedsrow").html("<legend class = 'pull-center'>No Favorites added yet. Click on the star icon to add favorites</legend ")
							.slideDown("200");
						});
					}
					else if(data.status == "Success"){
						feeds = data.message;
						pages = Math.ceil(feeds.length/9);
						page = 1;
						display();
					}
				},
				error:function(){
					
				}
			});
		});
	});
	$("body").delegate(".star","click",function(){
		if($(this).find("img").attr("src") == "images/full_star.png"){
			$(this).find("img").attr("src","images/ajax-loader.gif");
			index = this.id;
			id = index.substr(4);
			console.log(id);
			data = {"id":id};
			$.ajax({
				url:"scripts/removefavorite.php",
				type:"POST",
				data:data,
				dataType:"JSON",
				success:function(data){
					if(data.status == "unauthenticated"){
						window.location = "index.php";
					}
					if(data.status == "Success"){
						$("#favorites").trigger('click');
					}
					else{
						$("#errordiv p").html(data.message);
						$("#errordiv").slideDown("300");
						$("#"+index).find("img").attr("src","images/full_star.png");
					}
				},
				error:function(){
					$("#errordiv p").html("Error Occurred");
					$("#errordiv").slideDown("300");
					$("#"+index).find("img").attr("src","images/full_star.png");
				}
			});
			return false;
		}
		console.log(this);
		$(this).find("img").attr("src","images/ajax-loader.gif")
		index = this.id;
		id = index.substr(4);
		console.log("starid:"+id);
		console.log(feeds[id]);
		title = feeds[id].title;
		description = feeds[id].description;
		link = feeds[id].link;
		thumbnail = feeds[id].thumbnail;
		date = feeds[id].date;
		source = feeds[id].source;
		data = {"title":title,"description":description,"link":link,"thumbnail":thumbnail,"date":date,"source":source};
		console.log(data);
		$.ajax({
			url:"scripts/addfavorite.php",
			data:data,
			type:"POST",
			dataType:"JSON",
			success:function(data){
				if(data.status == "Success"){
					$("#"+index).find("img").attr("src","images/full_star.png");
				}
				else{
					$("#errordiv p").html(data.message).slideDown("300");
					$("#"+index).find("img").attr("src","images/empty_star.png");
				}
			},
			error:function(){
				$("#errordiv p").html("Error Occurred").slideDown("300");
				$("#"+index).find("img").attr("src","images/empty_star.png");
			}
		});
		return false;
	});
	
	
	
	$(".magazine:eq(0)").parent().addClass("active").find(".subMagazine").slideDown("700")
	.find(".sources").each(function(){
		url = this.id;
		firsturl.push(url);
	});
	$(".controls").click(function(){
		$(".controls").hide();
		if(this.id == "next"){
			page++;
		}
		else{
			page--;
		}
		
		$("#feedsrow,.controls").fadeOut('300',function(){
			$("#body-loading").show();
			display();
		});
	});
	data = "";
	for(i = 0; i < firsturl.length; i++){
		data += "url%5B%5D="+encodeURIComponent(firsturl[i]);
		console.log(i);
		if(i!=(firsturl.length)-1){
			data+="&";
		}
	}
	$("#feedsrow").hide();
	$("#body-loading").fadeIn("200");
	$.ajax({
		url:"scripts/getfeed.php",
		type:"GET",
		data:data,
		dataType:"JSON",
		success:function(data){
			console.log(data);
			feeds = data.message;
			len = feeds.length;
			pages = Math.ceil(len/9); 
			page = 1;
			display();
		},
		error:function(){
			
		}
	})
	$(".magazine").click(function(){
		$(".sources").css("background-color","white");
		if($(this).parent().find(".subMagazine:visible").length<=0 && this.id !== "favorites"){
			$("#feedsrow,.controls").fadeOut("200",function(){$("#body-loading").fadeIn("200");})
			$(".subMagazine:visible").slideUp("150",$.proxy(function(){
				$(".magazine").parent().removeClass("active");
				urls = new Array();
				$(this).parent().find(".subMagazine").find(".sources").each(function(){
					url = this.id;
					urls.push(url);
				});
				data = "";
				for(i = 0; i < urls.length; i++){
					data += "url%5B%5D="+encodeURIComponent(urls[i]);
					if(i!=(urls.length)-1){
						data+="&";
					}
				}
				$.ajax({
					url:"scripts/getfeed.php",
					type:"GET",
					data:data,
					dataType:"JSON",
					success:function(data){
						if(data.status == "unauthenticated"){
							window.location = "index.php";
						}
						feeds = data.message;
						len = feeds.length;
						pages = Math.ceil(len/9); 
						page = 1;
						display();
					},
					error:function(){
						
					}
				});
				
				
			$(this).parent().addClass("active").find(".subMagazine").slideDown("400");
			},this));
		}
		return false;
	});
	
	$(".sources").click(function(){
		if($(this).hasClass("siteselected") === true){
			return false;
		}
		$(".sources").css("background-color","white").removeClass("siteselected");
		$("#feedsrow,.controls").fadeOut("300",function(){
			$("#body-loading").fadeIn("200");
		});
		$(this).css("background-color","lightblue").addClass("siteselected");
		id = this.id;
		data = {"url[]":id};
		$.ajax({
			url:"scripts/getfeed.php",
			type:"GET",
			data:data,
			dataType:"JSON",
			success:function(data){
				if(data.status == "unauthenticated"){
					window.location = "index.php";
				}
				if(data.status == "Success"){
					feeds = data.message;
					page = 1;
					pages = Math.ceil(feeds.length/9);
					display();
				}
				else{
					$("#errordiv p").html(data.message).parent().slideDown();
					$("#body-loading").fadeOut("300",function(){
					});
				}
			},
			error:function(){
				
			}
		});
	});
});