    <div class = "modal hide fade" id = "privacypolicy">
    	<div class = "modal-header">
    		<h2 class = 'pull-center'>Privacy Policy</h2>
    	</div>
    	<div class = "modal-body">
    		<p class = 'doctext'>When accessing our Website, GridFeeds will learn certain information
    		 about you during your visit. How we will handle information we learn 
    		 about you depends upon what you do when visiting our site.
			If you visit our site to read or download information on our pages, 
			we collect and store only the following information about you:
			</p>
			<ol type = "1">
			<li><p>
				The name of the domain from which you access the Internet.
			</p></li>
			<li><p>
				The date and time you access our site. 
			</p></li>
			<li><p>
				The Internet address of the website you used to link directly to our site.
			</p></li>
			<li><p>
				Username.
			</p></li>
			<li><p>
				Password.
			</p></li>
			<li><p>
				Email Address.
			</p></li>
			</ol>
			
			<p class = 'doctext'>
				If you identify yourself by sending us an e-mail containing personal information, 
				then the information collected will be solely used to respond to your message.
			</p>
			<p class = 'doctext'>
				The information collected is for statistical purposes. GridFeeds may use software 
				programs to create summary statistics, which are used for such purposes as assessing 
				the number of visitors to the different sections of our site, what information is of
				 most and least interest, determining technical design specifications, and 
				 identifying system performance or problem areas.	
			</p>
			<p class = 'doctext'>
				For site security purposes and to ensure that this service remains available to all 
				users, GridFeeds uses software programs to monitor network traffic to identify unauthorized
				 attempts to upload or change information, or otherwise cause damage.
			</p>
			<p class = 'doctext'>
				GridFeeds will not obtain personally-identifying information about you when you visit our 
				site, unless you choose to provide such information to us, nor will such information be 
				sold or otherwise transferred to unaffiliated third parties without the approval of the 
				user at the time of collection.
			</p>

    	</div>
    	<div class = "modal-footer">
    		<a href = "#" class = "btn btn-primary pull-right" data-dismiss="modal" aria-hidden="true">
    		Close
    		</a>
    	</div>
    </div>